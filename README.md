# This a VITE + react + TS project

This project is a simple project that uses VITE, React and TypeScript.

# Start the project locally

To start the project locally, you need to run the following command:

```bash
yarn install
```

Then, you can run the following command:

```bash
yarn dev
```

# Build the project

To build the project, you need to run the following command:

```bash
yarn build
```

# Design system

This project uses the following design system:

- [Tailwind CSS](https://tailwindcss.com/) as a utility-first CSS framework
- [DSFR](https://react-dsfr.codegouv.studio/) as a design system

# Wiki

A wiki is available detailing setup [here](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/-/wikis/pages)
