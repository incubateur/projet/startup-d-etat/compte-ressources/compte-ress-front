## [1.36.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.36.0...v1.36.1) (2024-09-11)


### Bug Fixes

* Refetch catalog on page change ([9e36add](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/9e36add3d85f917667593946a9e3b8558064cf23))

# [1.36.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.35.1...v1.36.0) (2024-09-03)


### Bug Fixes

* Change key cycle4 in filter list ([0cf2226](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/0cf22269bfbcfccffe984434730b4f2310886631))
* Improve filter keys ([a5a4316](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/a5a431621615320d76df31ff4d056861f958aeef))
* Remove discipline title from filter ([b93d6db](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/b93d6db59fe31b3cc8ada94965cb1c1707a1ce75))
* Remove inclusive writing ([84f0810](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/84f08104ada07722e9b60ecaa3fe04a5ca11bb20))


### Features

* Remove duplicated profile ([06322b2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/06322b2c0528eece7410956012fec9d0aaa403f2))

## [1.35.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.35.0...v1.35.1) (2024-08-08)


### Bug Fixes

* Caclculate price from title ([465501a](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/465501a22298eb63c4f4e01bc2e10450ad37a201))

# [1.35.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.34.2...v1.35.0) (2024-08-08)


### Features

* Filter catalog request with prices ([9252444](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/925244482cb9d13a6cb0ad8a64537dffdd6fdb43))

## [1.34.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.34.1...v1.34.2) (2024-08-08)


### Bug Fixes

* Fetch 200 items for orders ([3e4c867](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/3e4c8670f6fe0b36fe86bb56d9d39e77090d03ef))

## [1.34.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.34.0...v1.34.1) (2024-08-08)


### Bug Fixes

* Optimistic updates ([0efde43](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/0efde43fb751cfdbf9a3e4ebda83e764b8354271))

# [1.34.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.33.0...v1.34.0) (2024-08-05)


### Features

* Improve notification layout ([d3fe3a4](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/d3fe3a4df33cbcf692f097f6e0cd4e9ac0060cab))

# [1.33.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.32.0...v1.33.0) (2024-08-01)


### Features

* Display title on transaction line ([59e4739](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/59e47399314ac7c5d8021f94cc61c0bcf93a1e04))

# [1.32.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.31.0...v1.32.0) (2024-08-01)


### Features

* Add concertation button ([fbffa2d](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/fbffa2d4455bb3ce50684594fa87ebd3d34dfd77))

# [1.31.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.30.0...v1.31.0) (2024-08-01)


### Features

* Display single transaction item on credit page ([f2cd8a1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/f2cd8a11da76a742a430984992e01850639cc99f))

# [1.30.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.29.2...v1.30.0) (2024-08-01)


### Features

* Improve filter list ([b550807](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/b5508073480f4664fbb0c635b888fafdbd1fb5d4))

## [1.29.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.29.1...v1.29.2) (2024-07-31)


### Bug Fixes

* Improve wording ([64bd0c6](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/64bd0c623945a2905f45a7accc496b8499e30c97))

## [1.29.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.29.0...v1.29.1) (2024-07-26)


### Bug Fixes

* Fetch 1000 orders before real pagination ([a021e78](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/a021e78d27362927bf356a9cd513447e10da592c))

# [1.29.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.28.0...v1.29.0) (2024-07-26)


### Features

* Improve user credits wording, EDUCCR-281 ([d264a2c](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/d264a2c5b301423f4fa845bfc50db221e186a831))

# [1.28.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.27.0...v1.28.0) (2024-07-26)


### Features

* Improve wording, EDUCCR-279 ([fce782c](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/fce782cdac698c05fd149fa3d2ccfe0a4e84f36d))

# [1.27.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.26.0...v1.27.0) (2024-07-25)


### Features

* Show credit charts only for admin, EDUCCR-271 ([ed4519b](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/ed4519b24dc2e4d03e001d1f4c5ed1cd82f9f1ae))

# [1.26.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.25.0...v1.26.0) (2024-07-25)


### Features

* Rework workflow and authorization for admin orders, EDUCCR-277 ([5168b4b](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/5168b4b8a50569260489361df5776f51b07f2fee))

# [1.25.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.24.0...v1.25.0) (2024-07-25)


### Features

* Add recharts and create two charts, EDUCCR-271 ([a79568e](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/a79568e5a3b55e8c706243588ddc4ae527e7d926))
* Rework wording, EDUCCR-267 ([55d95ec](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/55d95ec770889a53ba92a7e4bafeea05d7a104bc))

# [1.24.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.23.2...v1.24.0) (2024-07-23)


### Features

* Change assigne wording ([2a3f3f7](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/2a3f3f74db8ccc0d0d141a02ea32e62dbb79b1a9))

## [1.23.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.23.1...v1.23.2) (2024-07-22)


### Bug Fixes

* Change status only for matching order ([f67a56f](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/f67a56f5ae8c4e92ee00feb8ce98c951d57206ec))

## [1.23.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.23.0...v1.23.1) (2024-07-22)


### Bug Fixes

* Order admin requests by status ([3045e8b](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/3045e8b58d87eaf1814fa7f8fdfe569d071a586b))

# [1.23.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.22.0...v1.23.0) (2024-07-22)


### Features

* **credits:** Fix amount display, feat/EDUCCR-262 ([1e04f87](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/1e04f879febe43758ce4dba29116a3f25448adf3))

# [1.22.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.21.1...v1.22.0) (2024-07-22)


### Features

* Improve order item disabling, EDUCCR-266 ([234a783](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/234a7830d71642bbd3d571027f1414c69b2fdd26))

## [1.21.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.21.0...v1.21.1) (2024-07-22)


### Bug Fixes

* Disable affected items in admin orders page, EDUCCR-266 ([89d84e0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/89d84e097cd9c3fa858e148e7fdda36a4602adaf))

# [1.21.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.20.0...v1.21.0) (2024-07-22)


### Features

* **filters:** Add default discipline children filters, feat/EDUCCR-240 ([a7cfbfa](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/a7cfbfaf9512c273f7226441b8f7bb33ceb7d18d))

# [1.20.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.19.0...v1.20.0) (2024-07-22)


### Features

* Disable checkbox on admin request page, EDUCC-266 ([79e18d2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/79e18d220ea1f13ad44b471bf38239007fba3ca2))

# [1.19.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.18.0...v1.19.0) (2024-07-22)


### Features

* **header:** Add real EN logo, EDUCRR-239 ([b9398f2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/b9398f2368168d0e1de021755e2bd3c5916956eb))

# [1.18.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.17.0...v1.18.0) (2024-07-22)


### Features

* **requests:** Imporve request resource status user, EDUCCR-248 ([814a39f](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/814a39f6800802441b6af993c2cd1bca60fc8c17))

# [1.17.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.16.1...v1.17.0) (2024-07-19)


### Features

* Add admin API to change order statuses, EDUCCR-249 ([7ef9ae6](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/7ef9ae679b300a3abb5ed08641b2e0b541775bad))

## [1.16.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.16.0...v1.16.1) (2024-07-18)


### Bug Fixes

* Get roles from user infos, EDUCCR-249 ([a43686c](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/a43686cb43d41c700a54facb6466c71151b6bcc3))

# [1.16.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.15.0...v1.16.0) (2024-07-18)


### Features

* Fetch admin request, EDUCCR-245 ([7b3a656](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/7b3a6565dc80e19589374702a92fb6434d8fc3b5))
* **orders:** Add orders layout and selection, EDUCCR-244 ([103010e](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/103010ee09c6317f5acf618020af0b3db84e649b))

# [1.15.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.14.0...v1.15.0) (2024-07-16)


### Features

* Add teacher requests page Feat/EDUCCR-233 ([36e94f5](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/36e94f54509a67023573b835df2ee8238c5ce979))

# [1.14.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.13.0...v1.14.0) (2024-07-16)


### Features

* Add user credits page, EDUCCR-142 ([712d3bf](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/712d3bf07be43cddbdfbb0a48a2b18b994078021))

# [1.13.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.12.0...v1.13.0) (2024-07-15)


### Features

* Add account popover, EDUCCR-236 ([ca7d28b](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/ca7d28bc07638d5b44ed037ea0312a8d92dbab17))
* Add user role based navigation, EDUCCR-228 ([121b631](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/121b631144ab9c555b0c587021d96fb958e4279b))
* Display teacher resources, EDUCCR-234 ([97fb02b](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/97fb02bc2d47ee7ee22a88df125f74957521a215))

# [1.12.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.11.0...v1.12.0) (2024-07-08)


### Features

* Improve global layout, EDUCCR-225 ([23c0ced](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/23c0ced39735c4c8cfddd360c95dd6c1c05fea37))

# [1.11.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.10.0...v1.11.0) (2024-07-08)


### Features

* **footer:** Add loader, EDUCCR-224 ([3032e22](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/3032e2237e629c1117683d8adc5c4e375aff566f))

# [1.10.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.9.4...v1.10.0) (2024-07-08)

### Features

- Improve header responsiveness, EDUCCR-225 ([600abca](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/600abca6b44a8860d356cbbf6bbcbeca90c95acc))

## [1.9.4](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.9.3...v1.9.4) (2024-07-05)

## [1.9.3](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.9.2...v1.9.3) (2024-07-05)

### Bug Fixes

- Add api namespace to fetch endpoints, EDUCCR-227 ([e3e807e](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/e3e807ee501fb05e204da87ee204b53b1bcb9f80))

## [1.9.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.9.1...v1.9.2) (2024-07-05)

### Bug Fixes

- Change keycloak prod cred, EDUCCR-227 ([30dc2e8](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/30dc2e8cc1539f96b75b2772978b52bf2438b8a3))

## [1.9.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.9.0...v1.9.1) (2024-07-04)

### Bug Fixes

- Hide application if keycloak fails ([0e03740](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/0e0374076a4276710e0dc94ff5809af356ad68f5))

# [1.9.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.8.0...v1.9.0) (2024-07-04)

### Features

- **footer:** Add footer, EDUCCR-222 ([8b5d43b](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/8b5d43bd6559d04eb98168580b40d63117c211df))

# [1.8.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.7.0...v1.8.0) (2024-07-04)

### Features

- Add filters, EDUCCR-196 ([13ebb7f](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/13ebb7f4899c132aea05c198d688de54c95bf33b))

# [1.7.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.6.0...v1.7.0) (2024-07-04)

### Features

- **cart:** Remove resource from cart action, EDUCCR-204 ([b27f225](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/b27f2257cfd5d9a8c90fbdb0da44be694f311d61))

# [1.6.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.5.0...v1.6.0) (2024-07-04)

### Features

- **cart:** Add cart validation and display alert for success and error, EDUCCR-203 ([e9d1de3](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/e9d1de328fc225a07ea09879502c91ecea361e65))

# [1.5.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.4.0...v1.5.0) (2024-07-03)

### Features

- Create ressource details page, EDUCCR-133 ([fa79c36](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/fa79c363fcc4ca0360b5b8cd96843b6d8629102f))

# [1.4.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.3.0...v1.4.0) (2024-07-03)

### Features

- **cart:** Add display for basket, EDUCCR-147 ([403ca28](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/403ca288bec3d9583845315c8e035999069fbe75))

# [1.3.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.2.0...v1.3.0) (2024-07-01)

### Features

- Display catalog cards with data, EDUCCR-132 ([6348407](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/63484077fb0b54caefc48100f7e9961d4b548e43))

# [1.2.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.1.0...v1.2.0) (2024-06-25)

### Features

- Add pagination and select component in catalog, EDUCCR-132 ([5bfb646](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/5bfb646515418d8b410d54dab89d849982ab6f18))

# [1.1.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.0.1...v1.1.0) (2024-06-24)

### Features

- Fetch fake datas, EDUCCR-134 ([9b89a52](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/9b89a52b6d9c33c6dd8340a568c19f1b65d0c3c6))

## [1.0.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/compare/v1.0.0...v1.0.1) (2024-06-20)

# 1.0.0 (2024-06-19)

### Features

- Add keyclock, EDUCCR-171 ([1024f97](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/1024f971463bc5d587b443494f77ce12b6e95eb0))
- Adding rooter and ui library ([b4eaa8a](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/b4eaa8a621914c99f6fd92322012a3dc485be1cd))
- Remove startDsfr function ([cc86e3b](https://gitlab.com/ljn/projects/education-nationale/compte-ress-front/commit/cc86e3b11e589ebf740a7d0f05ddc1afab630338))
