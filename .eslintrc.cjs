module.exports = {
  root: true,
  env: { browser: true, es2020: true },
  extends: [
    'eslint:recommended',
    'airbnb',
    'airbnb-typescript',
    'plugin:import/recommended',
    'plugin:jsx-a11y/recommended',
    'plugin:react/recommended',
    'plugin:react/jsx-runtime',
    'plugin:react-hooks/recommended',
    'prettier',
  ],
  ignorePatterns: ['dist', '.eslintrc.cjs'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: './tsconfig.json',
  },
  parser: '@typescript-eslint/parser',
  plugins: ['no-relative-import-paths', 'react-refresh', 'react', '@typescript-eslint', 'prettier'],
  rules: {
    'import/no-extraneous-dependencies': 'off',
    'import/prefer-default-export': 'off',
    'import/no-duplicates': ['error', { 'prefer-inline': true }],
    'no-relative-import-paths/no-relative-import-paths': ['error', { rootDir: 'src' }],
    '@typescript-eslint/consistent-type-imports': 'error',
    'import/order': [
      'error',
      {
        groups: ['builtin', 'external', 'sibling'],
        pathGroups: [
          {
            pattern: '**/enums/**',
            group: 'sibling',
            position: 'before',
          },
          {
            pattern: '**/types/**',
            group: 'sibling',
            position: 'before',
          },
          {
            pattern: '**/hooks/**',
            group: 'sibling',
            position: 'before',
          },
          {
            pattern: '**/services/**',
            group: 'sibling',
            position: 'before',
          },
          {
            pattern: '**/server/**',
            group: 'sibling',
            position: 'before',
          },
          {
            pattern: '**/components/**',
            group: 'sibling',
            position: 'before',
          },
        ],
        'newlines-between': 'always',
        alphabetize: {
          order: 'asc',
          caseInsensitive: true,
        },
      },
    ],
    'react/react-in-jsx-scope': 'off',
    'react/require-default-props': 'off',
    'no-console': ['warn', { allow: ['error'] }],
    'react/function-component-definition': [
      2,
      {
        namedComponents: 'arrow-function',
      },
    ],
    'react-refresh/only-export-components': ['warn', { allowConstantExport: true }],
  },
  settings: {
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
    'import/resolver': {
      typescript: {},
    },
    react: {
      version: 'detect',
    },
  },
};
