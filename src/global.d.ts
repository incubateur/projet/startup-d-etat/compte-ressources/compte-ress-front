import type { defaultNS, resources } from 'assets/locales/i18n';

declare module 'i18next' {
  interface CustomTypeOptions {
    defaultNS: typeof defaultNS;
    resources: (typeof resources)['fr'];
  }
}
