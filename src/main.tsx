import { startReactDsfr } from '@codegouvfr/react-dsfr/spa';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { Link, type LinkProps } from '@tanstack/react-router';
import React from 'react';
import ReactDOM from 'react-dom/client';

import AuthProvider from 'shared/components/providers/authProvider';
import ConfigProvider from 'shared/components/providers/configProvider';

import 'assets/locales/i18n';
import 'index.css';

const queryClient = new QueryClient();

startReactDsfr({ defaultColorScheme: 'light', Link });

declare module '@codegouvfr/react-dsfr/spa' {
  interface RegisterLink {
    Link: (props: LinkProps) => JSX.Element;
  }
}

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <ConfigProvider>
        <AuthProvider />
      </ConfigProvider>
    </QueryClientProvider>
  </React.StrictMode>
);
