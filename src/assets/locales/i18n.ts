import * as i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import localesFr from 'assets/locales/fr-FR';

export const defaultNS = 'root';

export const resources = {
  fr: localesFr,
};

i18n.use(initReactI18next).init({
  resources,
  fallbackLng: 'fr',
  defaultNS,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
