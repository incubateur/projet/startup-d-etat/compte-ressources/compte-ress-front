import * as account from 'assets/locales/fr-FR/account.json';
import * as cart from 'assets/locales/fr-FR/cart.json';
import * as catalog from 'assets/locales/fr-FR/catalog.json';
import * as root from 'assets/locales/fr-FR/root.json';

const localesFr = { root, catalog, cart, account };

export default localesFr;
