import { Header } from '@codegouvfr/react-dsfr/Header';

const App = () => (
  <Header
    brandTop={
      <>
        INTITULE
        <br />
        OFFICIEL
      </>
    }
    homeLinkProps={{
      href: '/',
      title: 'Accueil - Nom de l’entité (ministère, secrétariat d‘état, gouvernement)',
    }}
    id="fr-header-simple-header"
    navigation={[
      {
        linkProps: {
          href: '#',
          target: '_self',
        },
        text: 'accès direct',
      },
      {
        isActive: true,
        linkProps: {
          href: '#',
          target: '_self',
        },
        text: 'accès direct',
      },
      {
        linkProps: {
          href: '#',
          target: '_self',
        },
        text: 'accès direct',
      },
      {
        linkProps: {
          href: '#',
          target: '_self',
        },
        text: 'accès direct',
      },
    ]}
  />
);

export default App;
