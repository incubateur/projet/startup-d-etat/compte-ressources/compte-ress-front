import { z } from 'zod';

export const metaDataSchema = z.object({
  current_page: z.number(),
  current_page_size: z.number(),
  page_size: z.number(),
  total_count: z.number(),
  total_page: z.number(),
});
