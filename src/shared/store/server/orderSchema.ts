import { z } from 'zod';

import { orderStatuses } from 'shared/enums/orderEnums';
import { userRoles } from 'shared/enums/userEnums';

export const orderInfoSchema = z.object({
  title: z.union([z.string(), z.array(z.string())]),
  educationalLevels: z.array(z.string()),
  date: z.string().optional(),
  resourceTypes: z.array(z.string()),
  editor: z.string().optional(),
  vignette: z.string(),
  statusHistory: z
    .object({
      assigned: z.string().optional(),
      ordered: z.string().optional(),
      submitted: z.string().optional(),
      validated: z.string().optional(),
    })
    .optional(),
  user: z.object({
    email: z.string(),
    firstname: z.string(),
    id: z.string(),
    lastname: z.string(),
    roles: z.array(z.enum(userRoles)),
    username: z.string(),
  }),
});

export const orderItemUser = z.object({
  user: z.object({
    email: z.string(),
    firstname: z.string(),
    id: z.string(),
    lastname: z.string(),
    roles: z.array(z.string()),
    username: z.string(),
  }),
});
export const orderItemSchema = z.object({
  id: z.string(),
  ark: z.string(),
  createdAt: z.string(),
  licenseQty: z.number(),
  licenseType: z.string(),
  modifiedAt: z.string(),
  price: z.number(),
  status: z.enum(orderStatuses),
  info: orderInfoSchema,
});

export const orderSchema = z.object({
  data: z.array(orderItemSchema),
  meta: z.object({
    current_page: z.number(),
    current_page_size: z.number(),
    page_size: z.number(),
    total_count: z.number(),
    total_page: z.number(),
  }),
});
