import type { Order, OrderStatus } from 'shared/types/orderTypes';

import { orderSchema } from 'shared/store/server/orderSchema';

import { getBaseUrl } from 'shared/utils/fetchUtils';

export const fetchOrderApi = async (
  token: string | undefined,
  statuses: Array<OrderStatus> = ['cart'],
  page: number = 1,
  size: number = 10
): Promise<{ data: Order; total: number } | null> => {
  if (!token) return null;

  try {
    const baseUrl = getBaseUrl();

    const paramsArray: Array<string> = [];

    statuses?.forEach((status) => {
      paramsArray.push(`status[]=${encodeURIComponent(status)}`);
    });

    const queryString = paramsArray.join('&');

    const response = await fetch(
      `${baseUrl}/cart?page=${page}&size=${size}&${queryString}&sort=-createdAt`,
      {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      }
    );

    const responseData = await response
      .json()
      .then((res) => ({ ...res, data: res.data.filter((item: { info: any }) => item.info.title) }));

    const zodData = orderSchema.safeParse(responseData);

    if (zodData.success) {
      return { data: zodData.data.data, total: responseData.meta.total_count };
    }

    return null;
  } catch (error) {
    console.error('Error fetching order', error);
    return null;
  }
};
