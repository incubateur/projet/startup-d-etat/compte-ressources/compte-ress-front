import type { LinkProps } from '@tanstack/react-router';

export const HEAD_OF_SCHOOL = 'chef_etablissement';
export const DIGITAL_REFERENT = 'referent_numerique';
export const ASSIGNMENT_MANAGER = 'responsable_affectation';
export const TEACHER = 'enseignant';

export const ADMIN_ROLES = [HEAD_OF_SCHOOL, ASSIGNMENT_MANAGER] as const;
export const userRoles = [HEAD_OF_SCHOOL, DIGITAL_REFERENT, ASSIGNMENT_MANAGER, TEACHER] as const;

export const ALL_ROLES = [...userRoles];

type UserRole = (typeof userRoles)[number]; // avoid circular dependency

export const userRoutes = [
  {
    key: 'account',
    to: '/compte',
    roles: [],
  },
  {
    key: 'profile',
    to: '/compte/profil',
    roles: ALL_ROLES,
  },
  {
    key: 'resources',
    to: '/compte/ressources',
    roles: ALL_ROLES,
  },
  {
    key: 'requests',
    to: '/compte/demandes',
    roles: ALL_ROLES,
  },
  {
    key: 'orders',
    to: '/compte/commandes',
    roles: [HEAD_OF_SCHOOL, ASSIGNMENT_MANAGER],
  },
  {
    key: 'credits',
    to: '/compte/credits',
    roles: ALL_ROLES,
  },
  {
    key: 'settings',
    to: '/compte/parametres',
    roles: ALL_ROLES,
  },
] as const satisfies Array<LinkProps & { roles: UserRole | Array<UserRole>; key: string }>;
