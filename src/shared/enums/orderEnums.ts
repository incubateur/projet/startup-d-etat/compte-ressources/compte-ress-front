export const PLACEHOLDER_CART_ITEM =
  'https://www.systeme-de-design.gouv.fr/img/placeholder.16x9.png';

export const RESOURCES_PER_PAGE = 10;

export const CART = 'cart';
export const SUBMITTED = 'submitted';
export const VALIDATED = 'validated';
export const ORDERED = 'ordered';
export const ASSIGNED = 'assigned';
export const CANCELED = 'canceled';
export const INVALIDATED = 'invalidated';

export const orderStatuses = [
  CART,
  SUBMITTED,
  VALIDATED,
  INVALIDATED,
  CANCELED,
  ORDERED,
  ASSIGNED,
] as const;
