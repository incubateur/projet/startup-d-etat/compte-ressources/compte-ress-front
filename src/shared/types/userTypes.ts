import { z } from 'zod';

import { userRoles } from 'shared/enums/userEnums';

export const userInfoSchema = z.object({
  sub: z.string(),
  family_name: z.string(),
  given_name: z.string(),
  email: z.string(),
  roles: z.array(z.enum(userRoles)).optional(),
});

export type UserRole = (typeof userRoles)[number];
export type UserInfo = z.infer<typeof userInfoSchema>;
