import { z } from 'zod';

export const configSchema = z.object({
  KEYCLOAK_URL: z.string(),
  BASE_URL: z.string(),
  API_VERSION: z.string().optional(),
  KEYCLOAK_CLIENT_ID: z.string().optional(),
  KEYCLOAK_CLIENT_REALM: z.string().optional(),
});

export type Config = z.infer<typeof configSchema>;
