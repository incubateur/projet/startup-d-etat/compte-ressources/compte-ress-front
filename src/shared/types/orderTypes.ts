import type { z } from 'zod';

import type { orderStatuses } from 'shared/enums/orderEnums';

import type {
  orderInfoSchema,
  orderItemSchema,
  orderSchema,
} from 'shared/store/server/orderSchema';

export type OrderStatus = (typeof orderStatuses)[number];

export type OrderInfo = z.infer<typeof orderInfoSchema>;
export type OrderItem = z.infer<typeof orderItemSchema>;
export type Order = z.infer<typeof orderSchema>['data'];
