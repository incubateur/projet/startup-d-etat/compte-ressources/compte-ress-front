import type { z } from 'zod';

import type { metaDataSchema } from 'shared/store/server/metaDataSchema';

export type MetaData = z.infer<typeof metaDataSchema>;

export type Pagination = {
  currentPage: number;
  pageSize: number;
  onChange: (pageNumber: number) => void;
  total?: number;
};
