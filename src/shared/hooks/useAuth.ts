import type Keycloak from 'keycloak-js';
import { create } from 'zustand';

import type { UserInfo, UserRole } from 'shared/types/userTypes';

type AuthState = {
  userRoles: Array<UserRole>;
  keycloak: Keycloak | null;
  loginFailed: boolean;
  userInfo: UserInfo | null;
  logUser: (keycloakInstance: Keycloak, userRoles: Array<UserRole>, userInfo: UserInfo) => void;
  setLoginFailed: () => void;
};

const useAuth = create<AuthState>((set) => ({
  userRoles: [],
  keycloak: null,
  loginFailed: false,
  userInfo: null,
  logUser: (keycloak, userRoles, userInfo) => set(() => ({ userRoles, keycloak, userInfo })),
  setLoginFailed: () => set(() => ({ loginFailed: true })),
}));

export default useAuth;
