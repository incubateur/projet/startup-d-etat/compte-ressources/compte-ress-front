import { useState } from 'react';

import type { Pagination } from 'shared/types/metaDataTypes';

const usePagination = (maxItemsPerPage: number = 10): Pagination => {
  const [currentPage, setCurrentPage] = useState(1);
  const onPageChange = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  return {
    currentPage,
    pageSize: maxItemsPerPage,
    onChange: onPageChange,
  };
};

export default usePagination;
