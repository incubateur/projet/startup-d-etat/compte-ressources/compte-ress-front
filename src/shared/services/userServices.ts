import { userRoutes } from 'shared/enums/userEnums';

import type { UserRole } from 'shared/types/userTypes';

export const hasRole = (
  userRoles: Array<UserRole>,
  role: UserRole | Array<UserRole> | undefined
) => {
  if (!role) return false;
  if (Array.isArray(role)) {
    return role.some((r) => userRoles.includes(r));
  }
  return userRoles.includes(role);
};

export const getUserNavItems = (userRoles: Array<UserRole>) =>
  userRoutes.filter((item) => hasRole(userRoles, item.roles));
