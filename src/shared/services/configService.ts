import type { Config } from 'shared/types/configTypes';

export type SetConfig = (config: Config) => void;

export const defaultConfig: Config = {
  BASE_URL: import.meta.env.VITE_BASE_URL as string,
  API_VERSION: import.meta.env.VITE_API_VERSION as string,
  KEYCLOAK_URL: import.meta.env.VITE_KEYCLOAK_URL as string,
  KEYCLOAK_CLIENT_ID: import.meta.env.VITE_KEYCLOAK_CLIENT_ID as string,
  KEYCLOAK_CLIENT_REALM: import.meta.env.VITE_KEYCLOAK_CLIENT_REALM as string,
};
class ConfigService {
  private static instance: ConfigService;

  private config: Config = defaultConfig;

  public static getInstance(): ConfigService {
    if (!ConfigService.instance) {
      ConfigService.instance = new ConfigService();
    }
    return ConfigService.instance;
  }

  public setConfig(config: Config) {
    this.config = { ...defaultConfig, ...config };
  }

  public getConfig(): Config {
    return this.config;
  }
}

const configService = ConfigService.getInstance();

export default configService;
