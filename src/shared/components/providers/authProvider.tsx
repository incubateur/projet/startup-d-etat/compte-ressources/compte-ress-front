import { createRouter, RouterProvider } from '@tanstack/react-router';
import Keycloak from 'keycloak-js';
import { Suspense, useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { TEACHER } from 'shared/enums/userEnums';

import { userInfoSchema } from 'shared/types/userTypes';

import useAuth from 'shared/hooks/useAuth';

import configService from 'shared/services/configService';

import Loader from 'shared/components/layout/Loader';

import { routeTree } from 'routeTree.gen';

const keycloakConfig = {
  clientId:
    import.meta.env.MODE === 'development'
      ? (import.meta.env.VITE_KEYCLOAK_CLIENT_ID as string)
      : 'compte-ressource',
  realm:
    import.meta.env.MODE === 'development'
      ? (import.meta.env.VITE_KEYCLOAK_REALM as string)
      : 'LaJavaness',
  'ssl-required': 'external',
  'public-client': true,
  'confidential-port': 0,
};

const router = createRouter({ routeTree, context: { auth: undefined } });
declare module '@tanstack/react-router' {
  interface Register {
    router: typeof router;
  }
}

const AuthProvider = () => {
  const { logUser, setLoginFailed, loginFailed, userRoles } = useAuth();
  const { t } = useTranslation();
  const isInitialized = useRef(false);

  useEffect(() => {
    const config = configService.getConfig();
    if (isInitialized.current || !config) return;
    const keycloakInstance = new Keycloak({
      ...keycloakConfig,
      url: config.KEYCLOAK_URL,
    });
    const initialize = () => {
      keycloakInstance
        .init({ onLoad: 'login-required' })
        .then(async () => {
          const loadedUserInfo = await keycloakInstance.loadUserInfo();
          const userInfo = userInfoSchema.parse(loadedUserInfo);
          const { roles = [TEACHER] } = userInfo;
          logUser(keycloakInstance, roles, userInfo);
        })
        .catch((err) => {
          setLoginFailed();
          console.error('Failed to initialize Keycloak', err);
        });
    };
    initialize();

    isInitialized.current = true;
  }, [logUser, setLoginFailed]);

  return (
    <Suspense fallback={<Loader />}>
      {loginFailed ? (
        <div className="flex w-svw items-center justify-center">{t('keycloakFailed')}</div>
      ) : (
        <RouterProvider router={router} context={{ userRoles }} />
      )}
    </Suspense>
  );
};

export default AuthProvider;
