import { useState } from 'react';

import { configSchema } from 'shared/types/configTypes';

import configService from 'shared/services/configService';

async function getConfig() {
  try {
    const response = await fetch('../envConfig.json');
    const data = await response.json().then(configSchema.parse);
    configService.setConfig(data);
  } catch (err) {
    console.error('Error fetching config', err);
  }
}

const ConfigProvider = ({ children }: { children: React.ReactElement }) => {
  const [configIsFetched, setConfigIsFetched] = useState(false);

  if (!configIsFetched) {
    getConfig().then(() => setConfigIsFetched(true));
  }

  // eslint-disable-next-line react/jsx-no-useless-fragment
  return configIsFetched ? <>{children}</> : null;
};

export default ConfigProvider;
