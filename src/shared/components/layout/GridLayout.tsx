type GridLayoutProps = {
  navigation: React.ReactNode;
  children: React.ReactNode;
};

const GridLayout = ({ navigation, children }: GridLayoutProps) => (
  <div className="mx-auto flex w-full max-w-7xl space-x-8 pt-4">
    {navigation}
    <div className="grow basis-3/4">{children}</div>
  </div>
);

export default GridLayout;
