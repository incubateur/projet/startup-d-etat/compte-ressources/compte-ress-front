import { Outlet, useLocation } from '@tanstack/react-router';
import { useTranslation } from 'react-i18next';

import { ADMIN_ROLES, userRoutes } from 'shared/enums/userEnums';

import useAuth from 'shared/hooks/useAuth';

import { getUserNavItems, hasRole } from 'shared/services/userServices';

import NavigationMenu from 'shared/components/dsfr/NavigationMenu';

const UserLayout = () => {
  const { userRoles } = useAuth();
  const { t } = useTranslation();

  const { pathname } = useLocation();

  const currentRoute = userRoutes.find((route) => route.to === pathname);
  const isCurrentRouteAllowed = hasRole(userRoles, currentRoute?.roles);

  const userNavItems = getUserNavItems(userRoles);

  const isAdmin = hasRole(userRoles, [...ADMIN_ROLES]);

  return (
    <div className="mx-auto flex w-full max-w-7xl space-x-8 pt-4">
      <aside>
        <NavigationMenu
          className="[&>*]:!shadow-none` p-0 [&>*]:!p-0"
          align="right"
          items={userNavItems.map((navItem) => ({
            linkProps: {
              to: navItem.to,
              activeOptions: { exact: false },
              // search: { page: 1 },
            },
            text: t(`menu.${isAdmin ? 'admin' : 'teacher'}.${navItem.key}`, { ns: 'account' }),
          }))}
          burgerMenuButtonText={null}
        />
      </aside>
      <div className="grow basis-3/4">
        {isCurrentRouteAllowed ? <Outlet /> : <div>{t('accessDenied', { ns: 'account' })}</div>}
      </div>
    </div>
  );
};

export default UserLayout;
