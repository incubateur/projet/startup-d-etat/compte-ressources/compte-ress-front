import { Skeleton as MUISkeleton, type SkeletonProps as MUISkeletonProps } from '@mui/material';

const Skeleton = ({ width, height, variant, className }: MUISkeletonProps) => (
  <MUISkeleton className={className} variant={variant} width={width} height={height} />
);

export default Skeleton;
