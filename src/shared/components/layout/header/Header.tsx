import { Header as DsfrHeader } from '@codegouvfr/react-dsfr/Header';
import { Link } from '@tanstack/react-router';
import { Trans, useTranslation } from 'react-i18next';

import useAuth from 'shared/hooks/useAuth';

import { useCartQuery } from 'modules/cart/store/server/cartQueries';

import { Button } from 'shared/components/dsfr';
import Notifications from 'shared/components/layout/header/Notifications';
import UserProfilButton from 'shared/components/layout/header/UserProfileButton';

const Header = () => {
  const { keycloak } = useAuth();
  const { data: cartData } = useCartQuery(keycloak?.token);
  const { t } = useTranslation('root');

  return (
    <DsfrHeader
      brandTop={<Trans i18nKey="header.title" components={{ br: <br /> }} />}
      serviceTagline={t('header.serviceTagline')}
      serviceTitle={t('header.serviceTitle')}
      homeLinkProps={{
        to: '/',
        title: 'Accueil -compte ressource',
      }}
      classes={{ toolsLinks: 'flex gap-4 items-center flex-end' }}
      quickAccessItems={[
        <div key={0} className="flex items-center justify-end gap-2">
          <UserProfilButton />
          <Notifications />
          <Link to="/panier" search={{ page: 1 }}>
            <Button priority="tertiary" iconId="fr-icon-shopping-cart-2-line" className="!mb-0">
              {cartData?.total}
            </Button>
          </Link>
        </div>,
      ]}
      navigation={[
        {
          linkProps: {
            to: '/catalogue',
          },
          text: t('catalog'),
        },
      ]}
      id="fr-header"
    />
  );
};

export default Header;
