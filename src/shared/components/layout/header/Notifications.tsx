import Button from '@codegouvfr/react-dsfr/Button';
import { Popover } from '@mui/material';
import { Link } from '@tanstack/react-router';
import { type MouseEvent, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ASSIGNMENT_MANAGER, HEAD_OF_SCHOOL } from 'shared/enums/userEnums';

import type { OrderItem } from 'shared/types/orderTypes';

import useAuth from 'shared/hooks/useAuth';

import { hasRole } from 'shared/services/userServices';

import {
  useOrderedOrdersQuery,
  useSubmittedOrdersQuery,
} from 'modules/account/store/server/orders/adminQueries';

const NotificationLink = ({ order, isOrder }: { order: OrderItem; isOrder?: boolean }) => (
  <Link
    to={`/compte/${isOrder ? 'commandes' : 'demandes'}`}
    key={order.id}
    className="bg-none pl-4 text-gray-900 no-underline hover:!bg-gray-200 hover:text-black"
  >
    <p className="font-bold">
      {order.info.user.firstname} {order.info.user.lastname}
    </p>
    <p className="font-light">{order.info.title}</p>
  </Link>
);

// This component (including queries) should be refacto to be DRY and more generic.
// Dont forget to remove optimistic updates from mutations (in cart and order status change)
const Notifications = () => {
  const { userRoles, keycloak } = useAuth();
  const { t } = useTranslation('root');
  const isDirector = hasRole(userRoles, HEAD_OF_SCHOOL);
  const isAssignment = hasRole(userRoles, ASSIGNMENT_MANAGER);
  const { data: submittedOrdersData } = useSubmittedOrdersQuery(
    isDirector ? keycloak?.token : undefined
  );

  const { data: orderedOrdersData } = useOrderedOrdersQuery(
    isAssignment ? keycloak?.token : undefined
  );

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const onOpenPopover = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'userMenu-popover' : undefined;

  if (!isAssignment && !isDirector) return null;

  const dataLength = submittedOrdersData?.data.length || orderedOrdersData?.data.length;

  return (
    <>
      <Button
        onClick={(event: MouseEvent<HTMLButtonElement>) => onOpenPopover(event)}
        className="relative !mb-0"
        priority="tertiary"
      >
        <span className="fr-icon-notification-3-line overflow-initial max-h-none max-w-full before:h-4 before:w-4 before:align-middle" />
        {dataLength ? (
          <span className="absolute -right-2 -top-2 h-4 w-4 rounded-full bg-primary text-xs text-white">
            {dataLength}
          </span>
        ) : null}
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <div className="max-h-[500px] w-[400px] overflow-auto p-5">
          <h3 className="font-bold text-gray-900">
            {t(`notifications.${isAssignment ? 'titleAssignment' : 'titleDirectory'}`)}
          </h3>
          <div className="flex flex-col gap-4">
            {submittedOrdersData?.data.map((order) => (
              <NotificationLink key={order.id} order={order} />
            ))}
            {orderedOrdersData?.data.map((order) => (
              <NotificationLink key={order.id} order={order} isOrder />
            ))}
          </div>
        </div>
      </Popover>
    </>
  );
};

export default Notifications;
