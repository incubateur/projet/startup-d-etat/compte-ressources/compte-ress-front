import { fr } from '@codegouvfr/react-dsfr';
import Button from '@codegouvfr/react-dsfr/Button';
import { Popover } from '@mui/material';
import { type MouseEvent, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ADMIN_ROLES } from 'shared/enums/userEnums';

import useAuth from 'shared/hooks/useAuth';

import { getUserNavItems, hasRole } from 'shared/services/userServices';

import { useWalletBalanceQuery } from 'modules/account/store/server/wallet/walletQueries';

import NavigationMenu from 'shared/components/dsfr/NavigationMenu';

import { cn } from 'shared/utils/styleUtils';

const UserProfilButton = () => {
  const { userInfo, userRoles, keycloak } = useAuth();
  const { t } = useTranslation();

  const { data: balance } = useWalletBalanceQuery(keycloak?.token);

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const onOpenPopover = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'userMenu-popover' : undefined;

  const userNavItems = getUserNavItems(userRoles);
  const isAdmin = hasRole(userRoles, [...ADMIN_ROLES]);

  return (
    <>
      <Button
        onClick={(event: MouseEvent<HTMLButtonElement>) => onOpenPopover(event)}
        className="relative !m-0 flex items-center gap-4 px-4 py-2 text-sm"
        priority="tertiary"
      >
        <span className="fr-icon-user-line block" />
        <div>
          <p>{t('hi', { name: userInfo?.given_name || '', ns: 'account' })}</p>
          <p className="font- text-xs text-gray-500">
            {userRoles.map((role) => t(`roles.${role}`, { ns: 'account' })).join(', ')}
          </p>
        </div>
        <span className={cn('fr-icon-arrow-down-s-line block', { 'rotate-180': !!anchorEl })} />
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <div className="w-[400px] p-5">
          <div className="font-bold">
            {userInfo?.given_name} {userInfo?.family_name}
          </div>
          <div className="text-slate-600">
            {userRoles.map((role) => t(`roles.${role}`, { ns: 'account' })).join(', ')}
          </div>
          <div className="text-slate-600">
            {t('userAccount.balance', { ns: 'account', count: balance || 0 })}
          </div>
          <NavigationMenu
            className={`${fr.cx()} p-0 [&>*]:!p-0 [&>*]:!shadow-none`}
            align="right"
            items={userNavItems.map((navItem) => ({
              linkProps: {
                to: navItem.to,
                activeOptions: { exact: true },
                // search: { page: 1 },
              },
              text: t(`menu.${isAdmin ? 'admin' : 'teacher'}.${navItem.key}`, { ns: 'account' }),
            }))}
            burgerMenuButtonText={null}
          />
          <Button
            onClick={() => keycloak?.logout()}
            priority="tertiary"
            iconId="fr-icon-logout-box-r-line"
            className="!mb-0 mt-4"
          >
            {t('logout')}
          </Button>
        </div>
      </Popover>
    </>
  );
};

export default UserProfilButton;
