import { Footer as DSFRFooter } from '@codegouvfr/react-dsfr/Footer';
import { useTranslation } from 'react-i18next';

const Footer = () => {
  const { t } = useTranslation('root');
  return (
    <DSFRFooter
      accessibility="partially compliant"
      contentDescription="Texte optionnel 3 lignes maximum.
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    Consectetur et vel quam auctor semper. Cras si amet mollis dolor."
      termsLinkProps={{
        href: '#',
      }}
      websiteMapLinkProps={{
        href: '#',
      }}
      license={
        <>
          {t('footer.licence')}{' '}
          <a href="https://github.com/codegouvfr/react-dsfr/blob/main/LICENSE">
            licence etalab-2.0
          </a>
        </>
      }
    />
  );
};

export default Footer;
