import { Backdrop, CircularProgress } from '@mui/material';

const Loader = () => (
  <Backdrop
    sx={{ background: '#fff', color: '#646cff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
    open
  >
    <CircularProgress color="inherit" />
  </Backdrop>
);

export default Loader;
