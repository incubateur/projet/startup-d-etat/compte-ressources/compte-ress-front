import { Link } from '@tanstack/react-router';

import { PLACEHOLDER_CART_ITEM } from 'shared/enums/orderEnums';

import type { CartItem } from 'modules/cart/types/cartTypes';
import type { OrderStatus } from 'shared/types/orderTypes';

import { findOrderTitle } from 'modules/account/services/adminServices';

import UserOrderItems from 'modules/account/components/OrderUsersList';
import { Badge, Checkbox, Card as DSFRCard } from 'shared/components/dsfr/index';

type CardProps = {
  onSelectCard?: (value: string, status?: OrderStatus) => void;
  orders: CartItem[];
  isChecked?: boolean;
  disabled?: boolean;
  value: string;
};
const Card = ({ isChecked, onSelectCard, orders, disabled, value }: CardProps) => {
  const title = findOrderTitle(orders[0]);

  return (
    <div className="flex w-full">
      {!!onSelectCard && (
        <Checkbox
          className="[&>fieldset]: m-1"
          small
          disabled={disabled}
          options={[
            {
              label: null,
              nativeInputProps: {
                value,
                checked: isChecked,
                onChange: (e) => onSelectCard(e.target.value as string, orders[0].status),
              },
            },
          ]}
          state="default"
        />
      )}
      <DSFRCard
        badge={
          <div className="flex items-center">
            {orders[0].info.resourceTypes?.length ? (
              <Badge className="!m-0">{orders[0].info.resourceTypes[0]}</Badge>
            ) : null}
          </div>
        }
        id={orders[0].ark}
        background
        className={`${isChecked && 'bg-blue-100'} my-2 w-full py-5`}
        border={false}
        // desc={
        // this card should be improved. Ticket is in backlog already
        //   <CartItemDescription
        //     licenseCount={orders.length}
        //     editor={orders[0].info.editor}
        //     isEditable={false}
        //     date={orders[0].info.date}
        //     credits={orders[0].price}
        //     educationalLevels={orders[0].info.educationalLevels}
        //   />
        // }
        horizontal
        classes={{
          end: 'hidden',
          start: 'hidden',
          desc: 'm-0',
          content: '!py-0',
          header: '!basis-[30%] !w-1/4',
        }}
        size="small"
        title={
          <Link to="/catalogue/$ressourceId" params={{ ressourceId: orders[0]?.ark }}>
            {title}
          </Link>
        }
        footer={<UserOrderItems orders={orders} />}
        imageAlt={title || ''}
        imageUrl={orders[0].info.vignette || PLACEHOLDER_CART_ITEM}
        titleAs="h3"
      />
    </div>
  );
};
export default Card;
