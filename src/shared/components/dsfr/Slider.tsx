import Slider from '@mui/material/Slider';
import { useState } from 'react';

type DoubleRangeSliderProps = {
  label: string;
  onChange: (value: number[]) => void;
  defaultValue: Array<number>;
};

const DoubleRangeSlider = ({ label, onChange, defaultValue }: DoubleRangeSliderProps) => {
  const [value, setValue] = useState<number[]>(defaultValue);

  const handleChange = (_: Event, newValue: number | number[]) => {
    setValue(newValue as number[]);
  };

  return (
    <div>
      <label className="mb-4" htmlFor="slider">
        {label}
      </label>
      <Slider
        getAriaLabel={() => label}
        value={value}
        onChange={handleChange}
        onChangeCommitted={(_, val) => onChange(val as number[])}
        valueLabelDisplay="auto"
        classes={{
          root: '!text-primary',
        }}
      />
    </div>
  );
};

export default DoubleRangeSlider;
