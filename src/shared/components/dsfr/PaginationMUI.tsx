/* eslint-disable react/jsx-props-no-spreading */
import MUIPagination, { type PaginationProps } from '@mui/material/Pagination';

const Pagination = (props: PaginationProps) => (
  <div className="mt-8 flex w-full justify-center">
    <MUIPagination {...props} />
  </div>
);

export default Pagination;
