import SideMenu, { type SideMenuProps } from '@codegouvfr/react-dsfr/SideMenu';

type NavigationMenusProps = {} & SideMenuProps;
const NavigationMenu = ({ items, align, className }: NavigationMenusProps) => (
  <SideMenu align={align} burgerMenuButtonText="ok" className={className} items={items} />
);
export default NavigationMenu;
