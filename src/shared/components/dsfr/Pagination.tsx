import {
  Pagination as DsfrPagination,
  type PaginationProps as DsfrPaginationProps,
} from '@codegouvfr/react-dsfr/Pagination';
import { useTranslation } from 'react-i18next';

import { cn } from 'shared/utils/styleUtils';

type PaginationProps = {
  onChange?: (pageNumber: number) => void;
  totalItems: number;
  pageSize: number;
  currentPage: number;
};

const Pagination = ({ pageSize, totalItems, currentPage, onChange }: PaginationProps) => {
  const { t } = useTranslation('root');

  const getPageLinkProps: DsfrPaginationProps['getPageLinkProps'] = (pageNumber: number) => ({
    title: `${t('pagination.page')} ${pageNumber}`,
    onClick: () => {
      onChange?.(pageNumber);
      window.scrollTo({ top: 0 });
    },
    to: '',
    search: { page: pageNumber },
    className: cn(
      `!text-current hover:!text-white hover:!bg-primary !border fr-pagination__link !cursor-pointer`,
      {
        '!border !text-white': currentPage === pageNumber,
      }
    ),
  });

  return pageSize < totalItems ? (
    <DsfrPagination
      classes={{
        list: 'justify-center mt-4 [&>li:first-child>a]:!bg-white [&>li:first-child>a]:!text-current ',
        link: `!text-current ${currentPage === 1 ? '!bg-white' : ''} ${currentPage === Math.ceil(totalItems / pageSize) && '!bg-white'} fr-pagination__link hover:!text-white hover:!bg-[#000091] !border  transition-colors `,
      }}
      showFirstLast
      count={Math.ceil(totalItems / pageSize)}
      defaultPage={currentPage}
      getPageLinkProps={getPageLinkProps}
    />
  ) : null;
};

export default Pagination;
