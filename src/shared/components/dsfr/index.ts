// direct export of components
export { fr } from '@codegouvfr/react-dsfr';
export { default as Badge } from '@codegouvfr/react-dsfr/Badge';
export { default as Button } from '@codegouvfr/react-dsfr/Button';
export { default as Card } from '@codegouvfr/react-dsfr/Card';
export { default as Checkbox } from '@codegouvfr/react-dsfr/Checkbox';
export { default as Select } from '@codegouvfr/react-dsfr/Select';
export { default as Tag } from '@codegouvfr/react-dsfr/Tag';

export { default as Pagination } from './Pagination';
export { default as Slider } from './Slider';
