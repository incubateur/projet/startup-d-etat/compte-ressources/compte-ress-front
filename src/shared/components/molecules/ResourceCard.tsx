import type { Ressource } from 'modules/catalog/types/catalogTypes';

import { Badge, Card, fr } from 'shared/components/dsfr';

import { cn } from 'shared/utils/styleUtils';

type ResourceCardProps = {
  resource: Ressource;
  className?: string;
};

const ResourceCard = ({ resource, className }: ResourceCardProps) => (
  <Card
    background
    className={cn('rounded-sm shadow-sm hover:cursor-pointer hover:shadow-md', className)}
    classes={{ end: fr.cx('fr-mt-0') }}
    imageAlt={resource.title}
    imageUrl={resource.vignette}
    size="small"
    title={resource.title}
    titleAs="h4"
    badge={resource.resourceTypes[0] ? <Badge>{resource.resourceTypes[0]}</Badge> : null}
    end={
      <div>
        <p className="mb-4 text-xs capitalize text-gray-500">
          {resource.date}
          {resource.editor ? ` - ${resource.editor}` : ''}
        </p>
        <p className="mb-2 text-xs capitalize text-gray-500">
          {resource.teachingDomains.slice(0, 1).join(', ')}
        </p>
        <p className="text-xs capitalize text-gray-500">
          {resource.educationalLevels.slice(0, 1).join(', ')}
        </p>
      </div>
    }
  />
);

export default ResourceCard;
