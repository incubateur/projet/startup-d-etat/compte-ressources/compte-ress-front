import { CircularProgress } from '@mui/material';

export const InnerLoader = () => (
  <div className="flex h-full w-full grow items-center justify-center">
    <CircularProgress color="inherit" />
  </div>
);
