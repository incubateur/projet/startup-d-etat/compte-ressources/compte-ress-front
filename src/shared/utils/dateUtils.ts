import dayjs from 'dayjs';

import { DATE_FRENCH_FORMAT } from 'shared/enums/dateEnums';

export const formatDate = (date: string, format = DATE_FRENCH_FORMAT) => dayjs(date).format(format);
