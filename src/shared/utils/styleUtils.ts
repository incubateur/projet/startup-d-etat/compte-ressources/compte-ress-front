import { type ClassValue, clsx } from 'clsx';
import { twMerge } from 'tailwind-merge';

// utility to merge tailwind classes and add additional classes

// example usage :
// className={cn("p-4 text-left", {
//             "bg-slate-200 text-primary hover:bg-slate-200": tab === selectedTab,
//           })}>{tab}</button>

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}
