import configService from 'shared/services/configService';

export const getBaseUrl = () => {
  const config = configService.getConfig();
  return config.BASE_URL + config.API_VERSION;
};

export const sliceItemsForPagination = <T>(
  items: Array<T> | undefined,
  page: number,
  pageSize: number
) => items?.slice(page * pageSize - pageSize, page * pageSize);
