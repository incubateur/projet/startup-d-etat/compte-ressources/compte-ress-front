import type { OrderItem } from 'shared/types/orderTypes';

export type CartItem = OrderItem;
