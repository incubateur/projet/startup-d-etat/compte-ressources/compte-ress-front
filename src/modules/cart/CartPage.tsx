import { createModal } from '@codegouvfr/react-dsfr/Modal';
import { useSearch } from '@tanstack/react-router';
import { useTranslation } from 'react-i18next';

import useCartPage from 'modules/cart/hooks/useCartPage';

import CartAlert from 'modules/cart/components/CartAlert';
import CartList from 'modules/cart/components/CartList';
import EmptyCart from 'modules/cart/components/EmptyCart';
import OrdersSummaryCard from 'modules/cart/components/OrdersSummaryCard';
import Pagination from 'shared/components/dsfr/Pagination';
import { InnerLoader } from 'shared/components/molecules/InnerLoader';

const modal = createModal({
  id: `modal`,
  isOpenedByDefault: false,
});
const CartPage = () => {
  const { t } = useTranslation('cart');
  const page = useSearch({ from: '/panier', select: (search) => search.page || 1 });

  const {
    allOrders,
    totalPrice,
    isValidationSuccess,
    creditAmount,
    total,
    isLoading,
    isDeleting,
    onValidateCart,
    onRemoveResourceFromCart,
    pagination,
  } = useCartPage(page);

  const onConfirmOrder = () => {
    modal.open();
    const HTMLTag = document.querySelector('html') as HTMLElement;
    HTMLTag.removeAttribute('data-fr-scrolling');
  };

  if (isLoading) return <InnerLoader />;

  return (
    <div className="mx-auto flex w-full max-w-7xl flex-col pt-4">
      <h1 className="fr-h2 mt-5">{t('title', { count: allOrders.length })}</h1>
      <CartAlert
        isValidationSuccess={isValidationSuccess}
        creditAmount={creditAmount}
        totalPrice={totalPrice}
      />
      <div className="container flex">
        {!allOrders?.length && !isLoading && <EmptyCart />}
        {allOrders?.length ? (
          <div className="fr-container">
            <CartList
              onRemoveResourceFromCart={onRemoveResourceFromCart}
              isDeleting={isDeleting}
              isLoading={isLoading}
              orders={allOrders || []}
            />
            {allOrders?.length ? (
              <div className="w-full bg-slate-200 p-5 text-right">
                {t('total.ressources', { count: allOrders?.length })}
                <span className="pr-1 font-bold">{totalPrice}</span>
                {t('total.credit', { count: creditAmount })}
              </div>
            ) : null}
            <modal.Component
              buttons={[
                {
                  doClosesModal: true,
                  children: t('cancel'),
                },
                {
                  iconId: 'ri-check-line',
                  children: t('sendCart'),
                  onClick: onValidateCart,
                },
              ]}
              title={t('summaryTitle')}
            >
              <CartList orders={allOrders} />
            </modal.Component>
          </div>
        ) : null}
        <OrdersSummaryCard
          creditAmount={creditAmount}
          onConfirmOrder={onConfirmOrder}
          totalPrice={totalPrice}
        />
      </div>

      <Pagination
        currentPage={page}
        pageSize={pagination.pageSize}
        totalItems={total || 0}
        onChange={pagination.onChange}
      />
    </div>
  );
};

export default CartPage;
