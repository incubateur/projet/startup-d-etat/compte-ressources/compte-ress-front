import { Alert } from '@codegouvfr/react-dsfr/Alert';
import { useTranslation } from 'react-i18next';

type CartAlertProps = {
  isValidationSuccess: boolean;
  creditAmount: number;
  totalPrice: number;
};

const CartAlert = ({ isValidationSuccess, creditAmount, totalPrice }: CartAlertProps) => {
  const { t } = useTranslation('cart');

  if (isValidationSuccess)
    return (
      <div className="m-8 mt-0">
        <Alert
          closable
          severity="success"
          description={t('alert.success.description')}
          title={t('alert.success.title')}
        />
      </div>
    );

  if (creditAmount < totalPrice)
    return (
      <div className="m-8 mt-0">
        <Alert
          closable
          description={t('alert.error.description')}
          severity="error"
          title={t('alert.error.title')}
        />
      </div>
    );

  return null;
};

export default CartAlert;
