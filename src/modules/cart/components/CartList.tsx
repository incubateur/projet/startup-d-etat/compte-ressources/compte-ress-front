import Badge from '@codegouvfr/react-dsfr/Badge';
import Button from '@codegouvfr/react-dsfr/Button';
import Card from '@codegouvfr/react-dsfr/Card';
import { Link } from '@tanstack/react-router';
import { useTranslation } from 'react-i18next';

import { PLACEHOLDER_CART_ITEM } from 'shared/enums/orderEnums';

import type { CartItem } from 'modules/cart/types/cartTypes';

import { findOrderTitle } from 'modules/account/services/adminServices';

import CartItemDescription from 'modules/cart/components/CartItemDescription';
import Loader from 'shared/components/layout/Loader';
import Skeleton from 'shared/components/layout/Skeleton';

type CartListProps = {
  orders: CartItem[];
  onRemoveResourceFromCart?: (id: string) => void;
  isDeleting?: boolean;
  isLoading?: boolean;
};

const CartList = ({ orders, isDeleting, onRemoveResourceFromCart, isLoading }: CartListProps) => {
  const { t } = useTranslation('cart');
  return (
    <div className="divide-y-5 grid grid-cols-1 divide-y">
      {isLoading
        ? Array.from({ length: 9 }, (_, i) => i + 1).map((i) => (
            <Skeleton className="my-5" key={i} height={300} variant="rectangular" />
          ))
        : orders?.map((order) => (
            <Card
              badge={
                order.info.resourceTypes?.length ? (
                  <Badge>{order.info.resourceTypes[0]}</Badge>
                ) : null
              }
              key={order.id}
              background
              className="py-5"
              classes={{
                desc: 'm-0',
              }}
              border={false}
              desc={
                <CartItemDescription
                  editor={order.info.editor}
                  isEditable={!!onRemoveResourceFromCart}
                  date={order.info.date as string}
                  credits={order?.price}
                  educationalLevels={order.info.educationalLevels}
                />
              }
              footer={
                onRemoveResourceFromCart ? (
                  <div className="flex gap-4">
                    <Button
                      priority="secondary"
                      onClick={() => onRemoveResourceFromCart(order?.id)}
                      size="medium"
                      disabled={isDeleting}
                      className="rounded-none"
                    >
                      {isDeleting && <Loader />}
                      {t('cartList.actions.delete')}
                    </Button>
                    <Button
                      priority="secondary"
                      size="medium"
                      disabled={isDeleting}
                      className="rounded-none"
                    >
                      {t('cartList.actions.consultation')}
                    </Button>
                  </div>
                ) : null
              }
              horizontal
              imageAlt={findOrderTitle(order)}
              imageUrl={order.info.vignette || PLACEHOLDER_CART_ITEM}
              size={onRemoveResourceFromCart ? 'medium' : 'small'}
              title={
                <Link to="/catalogue/$ressourceId" params={{ ressourceId: order?.ark }}>
                  {order.info.title}
                </Link>
              }
              titleAs="h3"
            />
          ))}
    </div>
  );
};

export default CartList;
