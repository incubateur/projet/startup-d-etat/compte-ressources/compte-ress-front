import type { FC } from 'react';
import { useTranslation } from 'react-i18next';

type CartItemDescriptionProps = {
  credits: number;
  licenseCount?: number;
  isEditable: boolean;
  editor: string | undefined;
  educationalLevels: Array<string> | undefined;
  date: string | undefined;
};
const CartItemDescription: FC<CartItemDescriptionProps> = ({
  licenseCount,
  date,
  educationalLevels,
  editor,
  credits,
  isEditable,
}) => {
  const { t } = useTranslation('cart');

  return (
    <span className="flex flex-col">
      <span className="flex justify-between">
        <span className="mb-2 text-base font-normal">
          <span className="mr-1">{!!licenseCount && licenseCount > 1 && licenseCount}</span>
          {t(`cartList.licenseType.INDIVIDUAL`)}
        </span>
        {isEditable && (
          <span className="mb-2 text-base font-normal">
            <span className="font-bold">{Number(credits)} </span>
            {t('total.credit', { count: Number(credits) })}
          </span>
        )}
      </span>
      <span className="mb-5 text-xs">
        {date} - {editor}
      </span>
      <span className="text-xs">{educationalLevels?.join(' / ')}</span>
    </span>
  );
};

export default CartItemDescription;
