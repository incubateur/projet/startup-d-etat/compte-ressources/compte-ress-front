import Card from '@codegouvfr/react-dsfr/Card';
import { Link } from '@tanstack/react-router';
import { useTranslation } from 'react-i18next';

const EmptyCart = () => {
  const { t } = useTranslation('cart');
  return (
    <div className="ml-8 mr-8 w-[900px]">
      <Card
        border
        horizontal
        size="small"
        desc={
          <span className="mt-0 flex flex-col">
            <span className="mb-3 w-full text-center text-base font-normal normal-case tracking-normal">
              {t('emptyCart.title')}
            </span>
            <span className="mb-3 w-full text-center">
              <Link to="/">{t('emptyCart.backToCatalog')}</Link>
            </span>
            <span className="w-full text-center">
              <Link to="/compte/demandes" search={{ page: 1 }}>
                {t('emptyCart.seeMoreRessources')}
              </Link>
            </span>
          </span>
        }
        title={null}
      />
    </div>
  );
};

export default EmptyCart;
