import Button from '@codegouvfr/react-dsfr/Button';
import type { FC } from 'react';
import { useTranslation } from 'react-i18next';

import useAuth from 'shared/hooks/useAuth';

import { useCartQuery } from 'modules/cart/store/server/cartQueries';

import { cn } from 'shared/utils/styleUtils';

type OrdersSummaryCardProps = {
  onConfirmOrder: () => void;
  totalPrice: number;
  creditAmount: number;
};

const OrdersSummaryCard: FC<OrdersSummaryCardProps> = ({
  onConfirmOrder,
  totalPrice,
  creditAmount,
}) => {
  const { t } = useTranslation('cart');
  const { keycloak } = useAuth();

  const { data } = useCartQuery(keycloak?.token);

  return (
    <div className="container w-[360px]">
      <div className="divide-y border border-gray-300 p-5">
        <h2 className="title">{t('total.ressources', { count: data?.total })}</h2>
        {totalPrice ? (
          <p className={cn('pt-2 font-bold', { 'text-red-500': totalPrice > creditAmount })}>
            {t('total.totalCredit', { count: totalPrice })}
          </p>
        ) : null}
        <p className="my-5 pt-5">{t('currentCreditAmount', { count: creditAmount })}</p>
        <Button
          disabled={!data?.data?.length || totalPrice > creditAmount}
          onClick={onConfirmOrder}
          priority="primary"
          size="large"
          className="rounded-none"
        >
          {t('sendCart')}
        </Button>
      </div>
    </div>
  );
};

export default OrdersSummaryCard;
