import { getBaseUrl } from 'shared/utils/fetchUtils';

export const flushRessourcesFromCart = async (token: string | undefined): Promise<void> => {
  if (!token) return;
  try {
    const baseUrl = getBaseUrl();
    await fetch(`${baseUrl}/cart`, {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
  } catch (err) {
    console.error('Error flushing cart', err);
  }
};

export const removeRessourceToCart = async (
  token: string | undefined,
  id: string
): Promise<void> => {
  if (!token) return;
  try {
    const baseUrl = getBaseUrl();
    await fetch(`${baseUrl}/cart/items/${id}`, {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
  } catch (err) {
    console.error('Error removing ressource from cart', err);
  }
};

export const submitCart = async (token: string | undefined): Promise<void> => {
  if (!token) return;
  try {
    const baseUrl = getBaseUrl();
    await fetch(`${baseUrl}/cart/submit`, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
  } catch (err) {
    console.error('Error submitting cart', err);
  }
};
