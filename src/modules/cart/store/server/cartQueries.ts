import { useMutation, useQuery } from '@tanstack/react-query';

import { CART } from 'shared/enums/orderEnums';

import {
  flushRessourcesFromCart,
  removeRessourceToCart,
  submitCart,
} from 'modules/cart/store/server/fetchCartApi';
import { fetchOrderApi } from 'shared/store/server/fetchOrderApi';

export const useCartQuery = (token: string | undefined, page: number = 1) =>
  useQuery({
    queryKey: ['cart', page],
    queryFn: () => fetchOrderApi(token, [CART]),
    enabled: !!token,
  });

export const useRemoveRessourceToCartMutation = (
  token: string | undefined,
  onSuccess: () => void
) =>
  useMutation({
    mutationFn: (id: string) => removeRessourceToCart(token, id),
    mutationKey: ['removeRessourceToCart'],
    onSuccess,
  });

export const useFlushRessourcesFromCartMutation = (
  token: string | undefined,
  onSuccess: () => void
) =>
  useMutation({
    mutationFn: () => flushRessourcesFromCart(token),
    mutationKey: ['flushRessourcesFromCart'],
    onSuccess,
  });

export const useCartSubmitMutation = (token: string | undefined, onSuccess?: () => void) =>
  useMutation({
    mutationFn: () => submitCart(token),
    mutationKey: ['validateCarte'],
    onSuccess,
  });
