import { useQueryClient } from '@tanstack/react-query';

import useAuth from 'shared/hooks/useAuth';
import usePagination from 'shared/hooks/usePagination';

import { useWalletBalanceQuery } from 'modules/account/store/server/wallet/walletQueries';
import {
  useCartQuery,
  useCartSubmitMutation,
  useRemoveRessourceToCartMutation,
} from 'modules/cart/store/server/cartQueries';

const useCartPage = (page: number) => {
  const { keycloak } = useAuth();

  const { data: cartData, isLoading: isLoadingCart } = useCartQuery(keycloak?.token, page);
  const queryClient = useQueryClient();
  const { data: balance, isLoading: isLoadingWallet } = useWalletBalanceQuery(keycloak?.token);

  const { onChange, pageSize } = usePagination();

  const totalPrice = cartData?.data?.reduce((sum, item) => sum + item.price, 0) || 0;

  const { mutate: removeResourceFromCart, isPending: isDeleting } =
    useRemoveRessourceToCartMutation(keycloak?.token, () => {
      queryClient.invalidateQueries({ queryKey: ['cart'] });
    });

  const onRemoveResourceFromCart = (resourceId: string) => {
    removeResourceFromCart(resourceId);
  };

  const { mutate: submitCart, isSuccess } = useCartSubmitMutation(keycloak?.token, () => {
    queryClient.invalidateQueries({ queryKey: ['cart'] });
    queryClient.invalidateQueries({ queryKey: ['submittedOrders'] });
    queryClient.invalidateQueries({ queryKey: ['orderedOrders'] });
  });

  const onValidateCart = () => {
    submitCart();
  };

  return {
    allOrders: cartData?.data || [],
    total: cartData?.total,
    totalPrice,
    isLoading: isLoadingCart || isLoadingWallet,
    creditAmount: balance || 0,
    onRemoveResourceFromCart,
    onValidateCart,
    isValidationSuccess: isSuccess,
    isDeleting,
    pagination: {
      onChange,
      pageSize,
    },
  };
};

export default useCartPage;
