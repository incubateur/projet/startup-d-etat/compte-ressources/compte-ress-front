import { useQueryClient } from '@tanstack/react-query';
import { Outlet, useChildMatches } from '@tanstack/react-router';
import { useEffect } from 'react';

import useCatalogPage from 'modules/catalog/hooks/useCatalogPage';

import Filters from 'modules/catalog/components/Filters';
import Ressources from 'modules/catalog/components/Ressources';
import SearchInput from 'modules/catalog/components/SearchInput';
import GridLayout from 'shared/components/layout/GridLayout';

const CatalogPage = () => {
  const { isLoading, sortedResources, totalRessources, onSortingOrderChange, setCreditsRange } =
    useCatalogPage();

  const queryClient = useQueryClient();
  const childMatches = useChildMatches();

  useEffect(() => {
    queryClient.invalidateQueries({ queryKey: ['catalog'] });
  }, [childMatches, queryClient]);

  if (childMatches.length) return <Outlet />;

  return (
    <GridLayout
      navigation={
        <div className="mt-4 flex-none">
          <SearchInput />
          <Filters setCreditsRange={setCreditsRange} />
        </div>
      }
    >
      <Ressources
        isLoading={isLoading}
        sortedResources={sortedResources}
        onSortingOrderChange={onSortingOrderChange}
        totalRessources={totalRessources}
      />
    </GridLayout>
  );
};

export default CatalogPage;
