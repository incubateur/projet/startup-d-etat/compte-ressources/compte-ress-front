import type { filterGroupsList } from 'modules/catalog/enums/filterEnums';

export type FilterName = (typeof filterGroupsList)[number];

export type FilterValue = React.Key;

export type CatalogSearch = Partial<{ [key in FilterName]: Array<FilterValue> }>;
