import type { z } from 'zod';

import type { sortingOrders } from 'modules/catalog/enums/sortingEnums';

import type {
  catalogSchema,
  classificationSchema,
  ressourceSchema,
  taxonPathSchema,
  taxonSchema,
} from 'modules/catalog/store/server/catalogSchemas';

import type { allEducationalLevelGroupsIds } from 'modules/catalog/utils/groupEducationalLevels';

export type Catalog = z.infer<typeof catalogSchema>;

export type RessourceSchema = z.infer<typeof ressourceSchema>;
export type ClassificationSchema = z.infer<typeof classificationSchema>;
export type TaxonSchema = z.infer<typeof taxonSchema>;
export type TaxonPathSchema = z.infer<typeof taxonPathSchema>;

export type Ressource = {
  vignette: string;
  id: string;
  title: string;
  description: string;
  date: string | undefined;
  editor: string | undefined;
  teachingDomains: Array<string>;
  educationalLevels: Array<string>;
  resourceTypes: Array<string>;
  groupOfLevels: Array<(typeof allEducationalLevelGroupsIds)[number]>;
  intendedEndUsers: Array<string>;
  price: number;
};

export type RessourcePayload = {
  ark: string;
  licenseType: string;
  licenseQty: number;
  price: number;
  info: {
    title: string;
    educationalLevels: Array<string>;
    date: string | undefined;
    resourceTypes: Array<string>;
    editor: string | undefined;
    vignette: string;
  };
};

export type SortingOrder = (typeof sortingOrders)[number];

export type RessourcesPagination = {
  currentPage: number;
  total: number;
  pageSize: number;
};
