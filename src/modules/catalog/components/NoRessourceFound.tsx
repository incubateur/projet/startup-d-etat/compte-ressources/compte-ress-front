import { useTranslation } from 'react-i18next';

const Ressources = () => {
  const { t } = useTranslation('catalog');

  return <div className="mx-auto flex max-w-7xl pt-4">{t('noRessourceFound')}</div>;
};

export default Ressources;
