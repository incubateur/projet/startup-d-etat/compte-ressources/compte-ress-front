import { Button } from '@codegouvfr/react-dsfr/Button';
import { Select } from '@codegouvfr/react-dsfr/Select';
import { getRouteApi, Link } from '@tanstack/react-router';
import { useTranslation } from 'react-i18next';

import { sortingOrders } from 'modules/catalog/enums/sortingEnums';

import type {
  Ressource,
  RessourcesPagination,
  SortingOrder,
} from 'modules/catalog/types/catalogTypes';

import NoRessourceFound from 'modules/catalog/components/NoRessourceFound';
import Pagination from 'shared/components/dsfr/PaginationMUI';
import Skeleton from 'shared/components/layout/Skeleton';
import ResourceCard from 'shared/components/molecules/ResourceCard';

import {
  defineRessourcesCurrentEndIndex,
  defineRessourcesCurrentStartIndex,
} from 'modules/catalog/utils/catalogUtils';

type RessourcesProps = {
  isLoading: boolean;
  sortedResources: Ressource[] | undefined;
  totalRessources: number;
  onSortingOrderChange: (order: SortingOrder) => void;
};

const route = getRouteApi('/catalogue');

const Ressources = ({
  isLoading,
  sortedResources,
  onSortingOrderChange,
  totalRessources,
}: RessourcesProps) => {
  const { t } = useTranslation('catalog');
  const pageParams = route.useSearch();
  const navigate = route.useNavigate();

  const onChange = async (_: any, page: number) => {
    window.scrollTo({ top: 0 });
    await navigate({ search: { ...pageParams, page } });
  };

  if (!sortedResources && !isLoading) return <NoRessourceFound />;

  return (
    <>
      <div className="flex items-center justify-between py-4">
        <p>
          {!isLoading &&
            t('resultsSummary', {
              start: defineRessourcesCurrentStartIndex({
                currentPage: pageParams.page,
                pageSize: 24,
              } as RessourcesPagination),
              end: defineRessourcesCurrentEndIndex({
                currentPage: pageParams.page,
                pageSize: 24,
                total: totalRessources,
              } as RessourcesPagination),
              count: totalRessources,
            })}
        </p>
        <div className="flex items-center gap-2">
          {t('sortBy')}
          <Select
            className="mt-0"
            label={null}
            nativeSelectProps={{
              name: 'sortingOrder',
              defaultValue: sortingOrders[0],
              onChange: (event) => onSortingOrderChange(event.target.value as SortingOrder),
            }}
          >
            <option value="" disabled hidden>
              {t('selectSortOption.placeholder')}
            </option>
            {sortingOrders.map((sortingOrder) => (
              <option key={sortingOrder} value={sortingOrder}>
                {t(`selectSortOption.${sortingOrder}`)}
              </option>
            ))}
          </Select>
        </div>
      </div>
      <div className="grid grid-cols-1 gap-8 md:grid-cols-2 lg:grid-cols-3">
        {isLoading
          ? Array.from({ length: 9 }, (_, i) => i + 1).map((i) => (
              <Skeleton key={i} height={300} variant="rounded" width={300} />
            ))
          : sortedResources?.map((ressource) => (
              <div key={ressource.id} className="basis-1/4">
                <Link
                  to="/catalogue/$ressourceId"
                  params={{ ressourceId: ressource.id }}
                  search={{ page: 1 }}
                >
                  <ResourceCard resource={ressource} />
                </Link>
              </div>
            ))}
      </div>
      <Pagination
        count={Math.ceil(totalRessources / 24)}
        page={pageParams.page}
        onChange={onChange}
        boundaryCount={2}
      />

      <Button
        className="float-right"
        priority="tertiary"
        onClick={() => window.scrollTo({ top: 0 })}
        iconId="fr-icon-arrow-up-line"
      >
        {t('scrollToTop')}
      </Button>
    </>
  );
};

export default Ressources;
