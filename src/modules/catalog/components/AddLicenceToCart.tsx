import { Button } from '@codegouvfr/react-dsfr/Button';
import { useTranslation } from 'react-i18next';

import type { licenceTypes } from 'modules/catalog/enums/licenceTypes';

import type { Ressource, RessourcePayload } from 'modules/catalog/types/catalogTypes';

type AddLicenceToCartProps = {
  resource: Ressource;
  type: (typeof licenceTypes)[number];
  addToCart: (ressource: RessourcePayload) => void;
};

const AddLicenceToCart = ({ type, resource, addToCart }: AddLicenceToCartProps) => {
  const { t } = useTranslation('catalog');

  const { price, id, title, vignette, resourceTypes, educationalLevels, editor, date } = resource;

  const isRegularLicence = type === 'regularLicence';

  const onAddToCartClick = () => {
    addToCart({
      ark: id,
      licenseType: 'individual',
      price: isRegularLicence ? price : 0,
      licenseQty: 1,
      info: {
        title,
        educationalLevels,
        date,
        resourceTypes,
        editor,
        vignette,
      },
    });
    window.scrollTo({ top: 0 });
  };

  return (
    <div
      className="flex items-center justify-between rounded-md border border-solid border-gray-400 p-4"
      id={t(`licence.href.${type}`)}
    >
      <div className="flex gap-16">
        <div className="flex flex-col gap-2">
          <h2 className="m-0 text-lg font-bold">{title}</h2>
          <p className="m-0">{t(`licence.${type}`)}</p>
          <p className="m-0 text-sm font-normal text-gray-500">
            {t('licence.ref')}: {id}
          </p>
        </div>
        {isRegularLicence ? (
          <div className="flex flex-col gap-2">
            <p className="text-sm font-bold text-gray-600">{t('licence.price')} :</p>
            <p>
              <span className="mr-1 font-bold">{price}</span>
              {t('licence.credits', { count: price })}
            </p>
          </div>
        ) : null}
      </div>
      <Button size="medium" onClick={onAddToCartClick}>
        {t('licence.addToCart')}
      </Button>
    </div>
  );
};

export default AddLicenceToCart;
