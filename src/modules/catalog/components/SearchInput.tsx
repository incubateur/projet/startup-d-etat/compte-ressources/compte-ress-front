import { SearchBar } from '@codegouvfr/react-dsfr/SearchBar';
import { getRouteApi } from '@tanstack/react-router';
import { useState } from 'react';
import { useDebounceCallback } from 'usehooks-ts';

import { TITLE } from 'modules/catalog/enums/filterEnums';

const route = getRouteApi('/catalogue');

const SearchInput = () => {
  const filters = route.useSearch();
  const navigate = route.useNavigate();
  const initialSearchValue = (filters[TITLE]?.[0] || '') as string;

  const [inputElement, setInputElement] = useState<HTMLInputElement | null>(null);

  const onSearchChange = (text: string) => {
    if (!text) {
      delete filters[TITLE];
      return navigate({ search: { ...filters, page: 1 } });
    }
    return navigate({ search: { ...filters, [TITLE]: [text], page: 1 } });
  };

  const debouncedOnSearch = useDebounceCallback(onSearchChange, 300);

  return (
    <SearchBar
      className="mb-8"
      onButtonClick={onSearchChange}
      renderInput={({ className, id, placeholder, type }) => (
        <input
          ref={setInputElement}
          className={className}
          id={id}
          placeholder={placeholder}
          type={type}
          defaultValue={initialSearchValue}
          // Note: The default behavior for an input of type 'text' is to clear the input value when the escape key is pressed.
          // However, due to a bug in @gouvfr/dsfr the escape key event is not propagated to the input element.
          // As a result this onChange is not called when the escape key is pressed.
          onChange={(event) => debouncedOnSearch(event.currentTarget.value)}
          // Same goes for the keydown event so this is useless but we hope the bug will be fixed soon.
          onKeyDown={(event) => {
            if (event.key === 'Escape') {
              inputElement?.blur();
            }
          }}
        />
      )}
    />
  );
};

export default SearchInput;
