import { Alert } from '@codegouvfr/react-dsfr/Alert';
import Button from '@codegouvfr/react-dsfr/Button';
import { CircularProgress } from '@mui/material';
import { useQueryClient } from '@tanstack/react-query';
import { Link, useParams } from '@tanstack/react-router';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { licenceTypes } from 'modules/catalog/enums/licenceTypes';

import useAuth from 'shared/hooks/useAuth';

import {
  useAddRessourceToCartMutation,
  useRessourceQuery,
} from 'modules/catalog/store/server/catalogQueries';

import AddLicenceToCart from 'modules/catalog/components/AddLicenceToCart';
import RessourceDetails from 'modules/catalog/components/RessourceDetails';

const RessourcePage = () => {
  const { t } = useTranslation('catalog');
  const auth = useAuth();
  const [ressourceAdded, setRessourceAdded] = useState(false);

  const ressourceId = useParams({
    from: '/catalogue/$ressourceId',
    select: (params) => params.ressourceId,
  });
  const queryClient = useQueryClient();

  const { mutate: addToCart } = useAddRessourceToCartMutation(auth.keycloak?.token, () => {
    setRessourceAdded(true);
    queryClient.invalidateQueries({ queryKey: ['cart'] });
  });

  const { data: ressource, isLoading } = useRessourceQuery(auth.keycloak?.token, ressourceId);

  if (isLoading)
    return (
      <div className="flex h-full w-full grow items-center justify-center">
        <CircularProgress color="inherit" />
      </div>
    );

  if (!ressource) return <div>No data</div>;

  return (
    <div className="w-full items-center justify-between scroll-smooth py-4">
      {ressourceAdded && (
        <Alert
          className="my-4"
          title={t('alert.title')}
          severity="success"
          closable
          onClose={() => setRessourceAdded(false)}
        />
      )}
      <div className="font-light">
        <Link className="text-gray-500" to="/catalogue">
          {t('catalogue')}
        </Link>
        <span className="px-1">/</span>
        <span className="">{ressource.title}</span>
      </div>
      <div className="mt-6 flex grow gap-8">
        <img className="contain w-80 object-cover" src={ressource.vignette} alt={ressource.title} />
        <RessourceDetails
          ressource={ressource}
          className="max-w-lg grow"
          seeMore={
            <a className="block w-fit whitespace-nowrap text-right" href="#details">
              {t('seeDetails')}
            </a>
          }
        >
          <h1 className="text-2xl font-bold">{ressource.title}</h1>
        </RessourceDetails>
        <div className="flex h-fit flex-col gap-4 rounded-md border border-gray-200 p-8">
          <a
            className="block w-fit whitespace-nowrap text-right"
            href={`#${t('licence.href.regularLicence')}`}
          >
            {t('licence.choseLicence')}
          </a>
          <a
            className="block whitespace-nowrap text-right"
            href={`#${t('licence.href.testLicence')}`}
          >
            {t('licence.testResource')}
          </a>
        </div>
      </div>
      <div className="mt-8">
        <h2 className="pb-2 text-xl font-bold">{t('ressource.description')}</h2>
        <p className="whitespace-pre-wrap">{ressource.description}</p>
      </div>
      <div className="mt-8">
        <h2 className="pb-2 text-xl font-bold">{t('licence.title')}</h2>
        <div className="flex flex-col gap-4">
          {licenceTypes.map((type) => (
            <AddLicenceToCart key={type} type={type} addToCart={addToCart} resource={ressource} />
          ))}
        </div>
      </div>
      <div className="mt-8" id="details">
        <h2 className="pb-2 text-xl font-bold">{t('details.title')}</h2>
        <RessourceDetails ressource={ressource} isSummary={false} />
      </div>
      <Button
        className="float-right"
        priority="tertiary"
        onClick={() => window.scrollTo({ top: 0 })}
        iconId="fr-icon-arrow-up-line"
      >
        {t('scrollToTop')}
      </Button>
    </div>
  );
};

export default RessourcePage;
