import { useTranslation } from 'react-i18next';

import type { Ressource } from 'modules/catalog/types/catalogTypes';

import { cn } from 'shared/utils/styleUtils';

type RessourceDetailsItemProps = {
  ressourceKey: keyof Ressource;
  children: React.ReactNode;
  isSummary?: boolean;
};

const RessourceDetailsItem = ({ ressourceKey, children, isSummary }: RessourceDetailsItemProps) => {
  const { t } = useTranslation('catalog');
  if (!children) return null;
  return (
    <p className={cn('m-0', { truncate: isSummary })}>
      <span className="text-sm font-light text-gray-700">{t(`ressource.${ressourceKey}`)}</span>
      <span className="px-1 text-sm font-light text-gray-700">:</span>
      <span className="">{children}</span>
    </p>
  );
};

type RessourceDetailsProps = {
  children?: React.ReactNode;
  seeMore?: React.ReactNode;
  ressource: Ressource;
  isSummary?: boolean;
  className?: string;
};

const RessourceDetails = ({
  children,
  ressource,
  isSummary = true,
  className,
  seeMore,
}: RessourceDetailsProps) => {
  const { t } = useTranslation('catalog');

  return (
    <div className={cn('flex flex-col gap-4', className)}>
      {children}
      <RessourceDetailsItem ressourceKey="resourceTypes" isSummary={isSummary}>
        {ressource.resourceTypes.join(' / ')}
      </RessourceDetailsItem>
      <RessourceDetailsItem ressourceKey="intendedEndUsers" isSummary={isSummary}>
        {ressource.intendedEndUsers.join(' / ')}
      </RessourceDetailsItem>
      <RessourceDetailsItem ressourceKey="teachingDomains" isSummary={isSummary}>
        {ressource.teachingDomains.join(' / ')}
      </RessourceDetailsItem>
      <RessourceDetailsItem ressourceKey="educationalLevels" isSummary={isSummary}>
        {ressource.educationalLevels.join(' / ')}
      </RessourceDetailsItem>
      <RessourceDetailsItem ressourceKey="groupOfLevels" isSummary={isSummary}>
        {ressource.groupOfLevels.map(
          (level, i) =>
            level && (
              <span key={level}>
                {t(`levels.${level}`)}
                {ressource.groupOfLevels[i + 1] ? ' / ' : ''}
              </span>
            )
        )}
      </RessourceDetailsItem>
      <RessourceDetailsItem ressourceKey="editor" isSummary={isSummary}>
        {ressource.editor}
      </RessourceDetailsItem>
      <RessourceDetailsItem ressourceKey="date" isSummary={isSummary}>
        {ressource.date}
      </RessourceDetailsItem>
      {!isSummary && (
        <RessourceDetailsItem ressourceKey="id" isSummary={isSummary}>
          {ressource.id}
        </RessourceDetailsItem>
      )}
      {seeMore}
    </div>
  );
};

export default RessourceDetails;
