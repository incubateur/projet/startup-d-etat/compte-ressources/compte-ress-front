import { Button } from '@codegouvfr/react-dsfr/Button';
import { Tag } from '@codegouvfr/react-dsfr/Tag';
import { getRouteApi } from '@tanstack/react-router';
import { Tree, type TreeProps } from 'antd';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import type { CatalogSearch, FilterName } from 'modules/catalog/types/filterTypes';

import { Slider } from 'shared/components/dsfr';

import { getFiltersFromSearchParams } from 'modules/catalog/utils/catalogUtils';
import {
  flatFilterList,
  getActiveFilters,
  getCheckedKeys,
  getFilterList,
} from 'modules/catalog/utils/filterList';

const route = getRouteApi('/catalogue');

type FiltersProps = {
  setCreditsRange: (value: [number, number]) => void;
};

const Filters = ({ setCreditsRange }: FiltersProps) => {
  const { t } = useTranslation('catalog');
  const searchParams = route.useSearch();
  const navigate = route.useNavigate();

  const filters = getFiltersFromSearchParams(searchParams);
  const [isViewMore, setIsViewMore] = useState(false);

  const onRemoveTagClick = (key: string, value: React.Key) => async () => {
    const filterName = key as FilterName;
    const searchFilters = {
      ...filters,
      [filterName]: filters[filterName]?.filter((v) => v !== value),
    };
    await navigate({ search: { ...searchFilters, page: 1 } });
  };

  const onRemoveAllClick = async () => {
    await navigate({
      search: {
        page: 1,
      },
    });
  };

  const onCheck: TreeProps['onCheck'] = async (checkedKeysValue) => {
    const searchFilters = {} as CatalogSearch;
    (checkedKeysValue as React.Key[]).forEach((key) => {
      const parentGroup = flatFilterList.find((item) => item.children.includes(key));
      if (parentGroup) {
        searchFilters[parentGroup.key] = [...(searchFilters[parentGroup.key] || []), key];
      }
    });
    await navigate({ search: { ...searchFilters, page: 1 } });
  };

  const onSliderChange = (value: number[]) => {
    const min = value[0] || 0;
    const max = value[1] || 100;
    setCreditsRange([min, max]);
  };

  const activeFilters = getActiveFilters(filters);

  const checkedKeys = getCheckedKeys(filters);

  const filterList = getFilterList(filters);

  const gte = searchParams.gte || 0;
  const lt = searchParams.lt || 100;

  return (
    <div className="w-64 space-y-8">
      {activeFilters.length > 0 && (
        <div className="space-y-2">
          <h2 className="text-sm font-medium text-gray-500">{t('filters.title')}</h2>
          <div className="space-y-1">
            {activeFilters.map(([key, value]) =>
              value.map((item) => (
                <Tag
                  key={item}
                  className="mr-1"
                  dismissible
                  small
                  nativeButtonProps={{
                    onClick: onRemoveTagClick(key, item),
                  }}
                >
                  {item.toString()}
                </Tag>
              ))
            )}
          </div>
          <Button onClick={onRemoveAllClick} priority="tertiary" size="small">
            {t('filters.removeAll')}
          </Button>
        </div>
      )}
      <div>
        <h2 className="mb-4 text-sm font-medium text-gray-500">{t('filters.improveResults')}</h2>
        <Tree
          checkable
          onCheck={onCheck}
          checkedKeys={checkedKeys}
          treeData={isViewMore ? filterList : filterList.filter((filter) => filter.isMain)}
        />
        {isViewMore && (
          <div className="m-4 mb-8">
            <Slider
              label={t('filters.sliderLabel')}
              onChange={onSliderChange}
              defaultValue={[gte, lt]}
            />
          </div>
        )}
        <Button priority="tertiary no outline" onClick={() => setIsViewMore(!isViewMore)}>
          {t(`filters.${isViewMore ? 'hide' : 'showMore'}`)}
        </Button>
      </div>
    </div>
  );
};

export default Filters;
