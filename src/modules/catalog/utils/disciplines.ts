const maternelle = [
  'Acquérir les premiers outils mathématiques',
  "Agir, s'exprimer, comprendre à travers l'activité physique",
  "Agir, s'exprimer, comprendre à travers les activités artistiques",
  'Explorer le monde',
  'Mobiliser le langage dans toutes ses dimensions',
];

const primarySchool = [
  'Français',
  'Mathématiques',
  'Langues vivantes (étrangères ou régionales)',
  'Langues vivantes',
  'EPS',
  'Enseignement artistique',
  'Questionner le monde / EMC',
  'Science et technologie',
  'Enseignements artistiques',
  'Histoire et géographie / EMC',
  'Sciences et technologie',
  'Vie de la classe',
  'Éducation artistique et culturelle',
  'Éducation au développement durable',
  "Éducation aux médias et à l'information",
  'Éducation musicale',
];

const college = [
  'Arts plastiques',
  'Enseignement moral et civique',

  'Français',
  'Histoire des arts',
  'Histoire et géographie',
  'Langues vivantes (étrangères ou régionales)',
  'Mathématiques',
  'Physique-chimie',
  'Sciences de la vie et de la Terre',
  'Sciences et technologie',
  'Vie de la classe',
  'Éducation artistique et culturelle',
  'Éducation au développement durable',
  "Éducation aux médias et à l'information",
  'Éducation musicale',
];

export const lyceeGeneralAndTechnologique = [
  'Enseignement moral et civique',
  'Français',
  'Histoire et géographie',
  'Langues vivantes (étrangères ou régionales)',
  'Mathématiques',
  'Numérique et sciences informatiques',
  'Éducation artistique et culturelle',
  'Éducation au développement durable',
];

export const lyceePro = [
  "Accompagnement de l'élève",
  'Enseignement moral et civique',
  'Français',
  'Histoire et géographie',
  'Langues vivantes (étrangères ou régionales)',
  'Mathématiques',
  'Pratiques professionnelles',
  'Éducation artistique et culturelle',
  'Éducation au développement durable',
];

export const allDisciplines: { title: string; classes: string[]; children: React.Key[] }[] = [
  {
    title: 'Maternelle',
    classes: ['petite section', 'moyenne section', 'grande section'],
    children: maternelle,
  },
  {
    title: 'Élémentaire',
    classes: ['cp', 'ce1', 'ce2', 'cm1', 'cm2'],
    children: primarySchool,
  },
  {
    title: 'Collège',
    classes: ['6e', '5e', '4e', '3e'],
    children: college,
  },
  {
    title: 'Lycée général et technologique',
    classes: [
      '2de générale et technologique',
      '1re générale et technologique',
      'terminale générale et technologique',
    ],
    children: lyceeGeneralAndTechnologique,
  },
  {
    title: 'Lycée professionnel',
    classes: ['CAP 1° année', 'CAP 2° année', '2de professionnelle', '1re professionnelle'],
    children: lyceeGeneralAndTechnologique,
  },
];

export const allDisciplinesTitle = allDisciplines.map((discipline) => discipline.title);

export const flatDisciplines = allDisciplines.flatMap((discipline) =>
  discipline.children.map((child) => `${child} ${discipline.title}`)
);
