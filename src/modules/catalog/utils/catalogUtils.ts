import { filterGroupsList } from 'modules/catalog/enums/filterEnums';

import type { Ressource, RessourcesPagination } from 'modules/catalog/types/catalogTypes';
import type { CatalogSearch } from 'modules/catalog/types/filterTypes';

export const defineRessourcesCurrentStartIndex = ({
  currentPage,
  pageSize,
}: Omit<RessourcesPagination, 'total'>) => (currentPage - 1) * pageSize + 1;

export const defineRessourcesCurrentEndIndex = ({
  currentPage,
  pageSize,
  total,
}: RessourcesPagination) =>
  Math.min(total, defineRessourcesCurrentStartIndex({ currentPage, pageSize }) - 1 + pageSize);

type RessourceTitle = Ressource['title'];

export const compareTitleAlphabetically = (a: RessourceTitle, b: RessourceTitle) => {
  const firstTitle = typeof a === 'string' ? a : a[0];
  const secondTitle = typeof b === 'string' ? b : b[0];
  return firstTitle.toLocaleLowerCase().localeCompare(secondTitle.toLocaleLowerCase());
};

export const compareDates = (a: Ressource['date'], b: Ressource['date']) => {
  if (!a || !b) return 0;
  return a.localeCompare(b);
};

const hashString = (string: string) => {
  let hash = 0;
  for (let i = 0; i < string.length; i += 1) {
    const char = string.charCodeAt(i);
    hash = Math.imul(hash, 31) + char;
    // eslint-disable-next-line no-bitwise
    hash = (hash & 0xffffffff) >>> 0; // Convert to 32-bit integer
  }
  return hash;
};

export const getRandomIntFromString = (string: string | Array<string>) => {
  let value: string;
  if (Array.isArray(string)) {
    [value] = string;
  } else {
    value = string;
  }
  const hash = hashString(value);
  // Ensures a positive integer and maps it between 1 and 100
  const randomInt = (Math.abs(hash) % 100) + 1;
  return randomInt;
};

export const getFiltersFromSearchParams = (searchParams: Record<string, unknown>) => {
  const filters = filterGroupsList.reduce((acc, group) => {
    const activeFilters = searchParams[group];
    if (activeFilters) {
      return {
        ...acc,
        [group]: activeFilters,
      };
    }
    return acc;
  }, {} as CatalogSearch);
  return filters;
};
