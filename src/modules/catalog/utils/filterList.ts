import type { TreeDataNode } from 'antd';

import { TITLE } from 'modules/catalog/enums/filterEnums';

import type { CatalogSearch, FilterName, FilterValue } from 'modules/catalog/types/filterTypes';

import { allDisciplines, flatDisciplines } from 'modules/catalog/utils/disciplines';

type FilterNode = Omit<TreeDataNode, 'key'> & { key: FilterName; isMain?: boolean; order: number };

export const filterList: Array<FilterNode> = [
  {
    title: 'Niveaux - classes',
    order: 1,
    key: 'classe',
    disabled: true,
    isMain: true,
    children: [
      {
        title: 'Cycle 1',
        key: 'cycle1',
        children: [
          { title: 'petite section', key: 'petite section' },
          { title: 'moyenne section', key: 'moyenne section' },
          { title: 'grande section', key: 'grande section' },
        ],
      },
      {
        title: 'Cycle 2',
        key: 'cycle2',
        children: [
          { title: 'cp', key: 'cp' },
          { title: 'ce1', key: 'ce1' },
          { title: 'ce2', key: 'ce2' },
        ],
      },
      {
        title: 'Cycle 3',
        key: 'cycle3',
        children: [
          { title: 'cm1', key: 'cm1' },
          { title: 'cm2', key: 'cm2' },
          { title: '6e', key: '6e' },
        ],
      },
      {
        title: 'Cycle 4',
        key: 'cycle4',
        children: [
          { title: '5eme', key: '5e' },
          { title: '4eme', key: '4e' },
          { title: '3eme', key: '3e' },
        ],
      },
      {
        title: 'Lycée général et technologique',
        key: 'Lycée général et technologiques',
        children: [
          { title: 'seconde', key: '2de générale et technologique' },
          { title: 'première', key: '1re générale et technologique' },
          { title: 'terminale', key: 'terminale générale et technologique' },
        ],
      },

      {
        title: 'Lycée professionnel',
        key: 'Lycée pro. - CAP',
        children: [
          { title: '1re année CAP', key: 'CAP 1° année' },
          { title: '2e année CAP', key: 'CAP 2° année' },
          { title: 'seconde', key: '2de professionnelle' },
          { title: 'première', key: '1re professionnelle' },
          { title: 'terminale', key: 'terminale professionnelle' },
        ],
      },
    ],
  },
  {
    title: 'Type de ressource',
    order: 3,
    isMain: true,
    key: 'type',
    disabled: true,
    children: [
      { title: 'Dictionnaires et encyclopédies', key: 'Dictionnaires et encyclopédies' },
      { title: 'Documentaires et presse', key: 'Documentaires et presse' },
      { title: 'Enseignement multimédias', key: 'Enseignement multimédias' },
      { title: 'Entraînement et accompagnement', key: 'Entraînement et accompagnement' },
      { title: 'Manuels numériques', key: 'Manuels numériques' },
      { title: 'Orientation', key: 'Orientation' },
      { title: 'Production pédagogique', key: 'Production pédagogique' },
    ],
  },
  {
    title: 'Type d’accès',
    order: 4,
    key: 'acces',
    disabled: true,
    children: [
      { title: 'En ligne', key: 'En ligne' },
      { title: 'Hors ligne', key: 'téléchargeable' },
    ],
  },
  {
    title: 'Publics',
    order: 5,
    key: 'public',
    disabled: true,
    children: [
      { title: 'Élève', key: 'élève' },
      { title: 'Enseignant', key: 'enseignant des disciplines' },
      { title: 'Documentaliste', key: 'enseignant documentaliste' },
      { title: 'Autre personnel', key: "personnel d'orientation" },
    ],
  },
];

export const flatFilterList = filterList.reduce(
  (acc, item) => {
    const children =
      item.children
        ?.map((child) => {
          const childChildren = child.children?.map((grandChild) => grandChild.key);
          return [...(childChildren || [child.key])];
        })
        .flat() || [];
    return [...acc, { key: item.key, children }];
  },
  [{ key: 'discipline', children: flatDisciplines }] as Array<{
    key: FilterName;
    children: Array<FilterValue>;
  }>
);

export const formatSelectedFilters = (filters: CatalogSearch) => {
  const selectedFilters: FilterValue[] = [];
  Object.values(filters).forEach((value) => {
    selectedFilters.push(...value);
  });
  return selectedFilters;
};

export const getCheckedKeys = (filters: CatalogSearch) =>
  Object.entries(filters)
    .filter(([key]) => key !== TITLE)
    .map(([, value]) => value)
    .filter(Boolean)
    .flat();

export const getActiveFilters = (filters: CatalogSearch) =>
  Object.entries(filters).filter(([key]) => key !== TITLE);

export const getFilterList = (filters: CatalogSearch): FilterNode[] => {
  const activeFilters = getActiveFilters(filters);

  const disciplineNode: FilterNode = {
    title: 'Discipline',
    order: 2,
    key: 'discipline',
    disabled: true,
    children: [],
    isMain: true,
  };
  const activeClasses = activeFilters.find(([key]) => key === 'classe')?.[1];
  let disciplineChildren = allDisciplines;
  if (activeClasses) {
    disciplineChildren = allDisciplines.filter((discipline) =>
      discipline.classes.some((classe) => activeClasses.includes(classe))
    );
    if (disciplineChildren.length === 1) {
      disciplineNode.children = disciplineChildren[0].children.map((child) => ({
        key: child,
        title: child,
      })) as Array<FilterNode>;
    }
  }
  disciplineNode.children = disciplineChildren.map((discipline) => ({
    title: discipline.title,
    key: discipline.title,
    children: discipline.children.map((child) => ({
      key: `${child} ${discipline.title}`,
      title: child,
    })),
  })) as Array<FilterNode>;

  return [...filterList, disciplineNode].sort((a, b) => a.order - b.order);
};
