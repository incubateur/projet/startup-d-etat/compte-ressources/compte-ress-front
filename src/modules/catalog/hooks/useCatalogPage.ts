import { getRouteApi } from '@tanstack/react-router';
import { useState } from 'react';

import { ID_ASC, ID_DESC } from 'modules/catalog/enums/sortingEnums';

import type { Ressource, SortingOrder } from 'modules/catalog/types/catalogTypes';

import useAuth from 'shared/hooks/useAuth';

import { useRessourcesQuery } from 'modules/catalog/store/server/catalogQueries';

import {
  compareDates,
  compareTitleAlphabetically,
  getFiltersFromSearchParams,
} from 'modules/catalog/utils/catalogUtils';

const route = getRouteApi('/catalogue');

const useCatalogPage = () => {
  const searchParams = route.useSearch();
  const navigate = route.useNavigate();
  const { keycloak } = useAuth();
  const [sortingOrder, setSortingOrder] = useState<SortingOrder>('idAsc');

  const filters = getFiltersFromSearchParams(searchParams);

  const lt = searchParams.lt || 100;
  const gte = searchParams.gte || 0;
  const creditsRange: [number, number] = [gte, lt];

  const { data, isLoading, isFetching } = useRessourcesQuery(
    keycloak?.token,
    filters,
    searchParams.page,
    creditsRange
  );

  const onPriceFilterChange = async (range: [number, number]) => {
    await navigate({ search: { ...searchParams, gte: range[0], lt: range[1] } });
  };

  const sortedResources: Ressource[] | undefined = data?.data?.sort((a, b) => {
    switch (sortingOrder) {
      case ID_ASC:
        return compareTitleAlphabetically(a.title, b.title);
      case ID_DESC:
        return compareTitleAlphabetically(b.title, a.title);
      case 'dateAsc':
        return compareDates(a.date, b.date);
      case 'dateDesc':
        return compareDates(b.date, a.date);
      default:
        return 0;
    }
  });

  const onSortingOrderChange = (order: SortingOrder) => {
    setSortingOrder(order);
  };

  return {
    isLoading: isLoading || isFetching,
    sortedResources,
    totalRessources: data?.meta.total_count || 24,
    setCreditsRange: onPriceFilterChange,
    onSortingOrderChange,
  };
};

export default useCatalogPage;
