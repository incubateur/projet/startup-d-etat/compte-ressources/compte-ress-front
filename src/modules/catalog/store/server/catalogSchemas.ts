import { z } from 'zod';

const identifierSchema = z.object({
  entry: z.string(),
  catalog: z.string(),
});

export const taxonSchema = z.object({
  entry: z.object({
    string: z.string(),
  }),
});

export const taxonPathSchema = z.object({
  taxon: z.union([taxonSchema, z.array(taxonSchema)]).optional(),
});

export const classificationSchema = z.object({
  purpose: z.object({
    label: z.string(),
  }),
  taxonPath: z.union([taxonPathSchema, z.array(taxonPathSchema)]).optional(),
});

export const leaningRessourceTypeSchema = z.object({
  label: z.string(),
});

export const descriptionSchema = z.object({
  string: z.string(),
});

const intendedEndUserRoleSchema = z.object({
  label: z.string(),
});

export const ressourceSchema = z.object({
  general: z.object({
    identifier: z.union([identifierSchema, z.array(identifierSchema)]),
    title: z.object({
      string: z.union([z.string(), z.array(z.string())]),
    }),
    description: z.union([descriptionSchema, z.array(descriptionSchema)]),
  }),
  classification: z.array(classificationSchema),
  lifeCycle: z.object({
    contribute: z.array(
      z.object({
        date: z
          .object({
            dateTime: z.string(),
          })
          .optional(),
        entity: z.string(),
        role: z.object({
          label: z.string(),
          source: z.string(),
          value: z.string(),
        }),
      })
    ),
  }),
  relation: z.object({
    resource: z.object({
      identifier: z.object({
        entry: z.string(),
      }),
    }),
  }),
  educational: z
    .union([
      z.string(),
      z.object({
        learningResourceType: z
          .union([leaningRessourceTypeSchema, z.array(leaningRessourceTypeSchema)])
          .optional(),
        intendedEndUserRole: z
          .union([intendedEndUserRoleSchema, z.array(intendedEndUserRoleSchema)])
          .optional(),
      }),
    ])
    .optional(),
});

export const catalogSchema = z.object({
  resources: z.object({
    lom: z.union([z.array(ressourceSchema), ressourceSchema]),
  }),
});
