import {
  type classifications,
  EDUCATIONAL_LEVELS,
  TEACHING_DOMAINS,
} from 'modules/catalog/enums/ressourceClassifications';

import type {
  ClassificationSchema,
  Ressource,
  RessourceSchema,
  TaxonSchema,
} from 'modules/catalog/types/catalogTypes';

import { getRandomIntFromString } from 'modules/catalog/utils/catalogUtils';
import { mapEducationalLevelToGroups } from 'modules/catalog/utils/groupEducationalLevels';

const findTaxon = (taxon: TaxonSchema | TaxonSchema[] | undefined) => {
  if (!taxon) return [];
  if (Array.isArray(taxon)) {
    return taxon.map((t) => t.entry.string);
  }
  return [taxon.entry.string];
};

const findClassificationValues = (
  classification: ClassificationSchema[],
  classificationLabel: (typeof classifications)[number]
) => {
  let teachingDomains: Array<string> = [];
  const teachingDomainsPaths = classification.find(
    (cl) => cl.purpose.label === classificationLabel
  )?.taxonPath;
  if (!teachingDomainsPaths) return [];
  if (Array.isArray(teachingDomainsPaths)) {
    teachingDomains = teachingDomainsPaths.map((path) => findTaxon(path.taxon)).flat();
  } else {
    teachingDomains = findTaxon(teachingDomainsPaths.taxon);
  }
  return teachingDomains;
};

const findEducationalValues = (
  educational: RessourceSchema['educational'],
  valueName: 'learningResourceType' | 'intendedEndUserRole'
) => {
  if (!educational || typeof educational === 'string') return [];
  let educationalValues: string[] = [];
  const educationalValuesArray = educational?.[valueName];
  if (!educationalValuesArray) return [];
  if (Array.isArray(educationalValuesArray)) {
    educationalValues = educationalValuesArray.map((type) => type.label);
  } else {
    educationalValues = [educationalValuesArray.label];
  }
  return educationalValues;
};

export const cleanFetchedRessource = (ressource: RessourceSchema): Ressource => {
  const { general, lifeCycle, classification, educational } = ressource;
  const { identifier, title } = general;

  const id = Array.isArray(identifier)
    ? identifier.find((i) => i.catalog === 'ark')?.entry || identifier[0].entry
    : identifier.entry;

  const newTitle: string = Array.isArray(title) ? title[0].string : title.string;

  const date = lifeCycle.contribute[0].date?.dateTime;
  const match = lifeCycle.contribute[0].entity.match(/^FN:(.*)$/m);
  const editor = match ? match[1].trim() : undefined;
  const teachingDomains = findClassificationValues(classification, TEACHING_DOMAINS);
  const educationalLevels = findClassificationValues(classification, EDUCATIONAL_LEVELS);
  const groupOfLevels: Ressource['groupOfLevels'] = [
    ...new Set(educationalLevels.map((level) => mapEducationalLevelToGroups[level]).flat()),
  ];

  const resourceTypes = findEducationalValues(educational, 'learningResourceType');
  const intendedEndUsers = findEducationalValues(educational, 'intendedEndUserRole');
  const description = Array.isArray(general.description)
    ? general.description[0].string
    : general.description.string;

  const newRessource: Ressource = {
    vignette: ressource.relation.resource.identifier.entry,
    id,
    title: newTitle,
    description,
    date,
    editor,
    teachingDomains,
    educationalLevels,
    resourceTypes,
    groupOfLevels,
    intendedEndUsers,
    price: getRandomIntFromString(newTitle),
  };
  return newRessource;
};

export const cleanFetchedRessources = (
  ressources: RessourceSchema[] | RessourceSchema
): Ressource[] => {
  if (Array.isArray(ressources)) {
    const cleanedRessources = ressources.map((ressource) => cleanFetchedRessource(ressource));
    return cleanedRessources;
  }
  return [cleanFetchedRessource(ressources)];
};
