import { keepPreviousData, useMutation, useQuery } from '@tanstack/react-query';

import type { RessourcePayload } from 'modules/catalog/types/catalogTypes';
import type { CatalogSearch } from 'modules/catalog/types/filterTypes';

import {
  addRessourceToCart,
  fetchRessourceApi,
  fetchRessourcesApi,
} from 'modules/catalog/store/server/catalogFetchApi';

export const useRessourcesQuery = (
  token: string | undefined,
  filters: CatalogSearch,
  page: number = 1,
  creditsRange: [number, number] = [0, 100]
) =>
  useQuery({
    queryKey: ['catalog', filters, page, creditsRange],
    queryFn: () => fetchRessourcesApi(token, filters, page, creditsRange),
    enabled: !!token,
    placeholderData: keepPreviousData,
  });

export const useRessourceQuery = (token: string | undefined, id: string) =>
  useQuery({
    queryKey: ['catalog', id],
    queryFn: () => fetchRessourceApi(token, id),
    enabled: !!token,
  });

export const useAddRessourceToCartMutation = (token: string | undefined, onSuccess: () => void) =>
  useMutation({
    mutationFn: (ressource: RessourcePayload) => addRessourceToCart(token, ressource),
    mutationKey: ['addRessourceToCart'],
    onSuccess,
  });
