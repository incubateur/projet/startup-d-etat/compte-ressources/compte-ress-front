import { XMLParser } from 'fast-xml-parser';

import type { Ressource, RessourcePayload } from 'modules/catalog/types/catalogTypes';
import type { CatalogSearch } from 'modules/catalog/types/filterTypes';
import type { MetaData } from 'shared/types/metaDataTypes';

import { catalogSchema, ressourceSchema } from 'modules/catalog/store/server/catalogSchemas';
import {
  cleanFetchedRessource,
  cleanFetchedRessources,
} from 'modules/catalog/store/server/catalogServices';
import { metaDataSchema } from 'shared/store/server/metaDataSchema';

import { allDisciplinesTitle } from 'modules/catalog/utils/disciplines';
import { getBaseUrl } from 'shared/utils/fetchUtils';

export const fetchRessourcesApi = async (
  token: string | undefined,
  filters: CatalogSearch,
  page: number,
  creditsRange: [number, number]
): Promise<{ data: Ressource[]; meta: MetaData } | null> => {
  if (!token) return null;

  const pattern = new RegExp(allDisciplinesTitle.join('|'), 'g');

  try {
    const baseUrl = getBaseUrl();
    const formattedFilters = Object.entries(filters).map(([key, value]) => ({
      key,
      value: value.map((val) => val.toString().replace(pattern, '').trim()),
    }));

    const response = await fetch(
      `${baseUrl}/resources/search?page=${page}&lt=${creditsRange[1]}&gte=${creditsRange[0]}`,
      {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(formattedFilters),
      }
    );

    const json = await response.json();
    const parser = new XMLParser();
    const parsedXML = parser.parse(json.data);

    const zodData = catalogSchema.safeParse(parsedXML.xml);
    const meta = metaDataSchema.safeParse(json.meta);

    if (zodData.success && meta.success) {
      return {
        data: cleanFetchedRessources(zodData.data.resources.lom),
        meta: meta.data,
      };
    }

    return null;
  } catch (error) {
    return null;
  }
};

export const fetchRessourceApi = async (
  token: string | undefined,
  id: string
): Promise<Ressource | null> => {
  if (!token) return null;
  try {
    const baseUrl = getBaseUrl();
    const response = await fetch(`${baseUrl}/resources?id=${id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/ json',
        Authorization: `Bearer ${token}`,
      },
    });

    const xmlData = await response.text();
    const xmlDataWithoutLom = xmlData.replace(/lom:/g, '');
    const parser = new XMLParser();
    const parsedXML = parser.parse(xmlDataWithoutLom);

    const zodData = ressourceSchema.safeParse(parsedXML.lom);

    if (zodData.success) {
      return cleanFetchedRessource(zodData.data);
    }

    return null;
  } catch (error) {
    return null;
  }
};

export const addRessourceToCart = async (
  token: string | undefined,
  ressource: RessourcePayload
): Promise<void> => {
  if (!token) return;
  try {
    const baseUrl = getBaseUrl();
    await fetch(`${baseUrl}/cart/items`, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(ressource),
    });
  } catch (err) {
    console.error('Error adding ressource to cart', err);
  }
};
