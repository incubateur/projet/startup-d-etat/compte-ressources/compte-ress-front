export const ID_ASC = 'idAsc';
export const ID_DESC = 'idDesc';
export const DATE_ASC = 'dateAsc';
export const DATE_DESC = 'dateDesc';

export const sortingOrders = [ID_ASC, ID_DESC, DATE_ASC, DATE_DESC] as const;
