export const TEACHING_DOMAINS = "domaine d'enseignement";
export const EDUCATIONAL_LEVELS = 'niveau éducatif détaillé';

export const classifications = [TEACHING_DOMAINS, EDUCATIONAL_LEVELS] as const;
