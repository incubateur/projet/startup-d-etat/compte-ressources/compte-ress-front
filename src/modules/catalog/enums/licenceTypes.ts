const TEST_LICENCE = 'testLicence';
const REGULAR_LICENCE = 'regularLicence';

export const licenceTypes = [TEST_LICENCE, REGULAR_LICENCE] as const;
