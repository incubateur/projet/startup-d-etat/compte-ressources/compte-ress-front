export const DISCIPLINE = 'discipline';
export const CLASSE = 'classe';
export const TYPE = 'type';
export const ACES = 'acces';
export const PUBLIC = 'public';
export const EDITOR = 'editeur';
export const TITLE = 'titre';
export const DESCRIPTION = 'description';
export const KEYWORD = 'keyword';
export const PAGE = 'page';

export const filterGroupsList = [
  DISCIPLINE,
  CLASSE,
  TYPE,
  ACES,
  PUBLIC,
  EDITOR,
  TITLE,
  DESCRIPTION,
  KEYWORD,
] as const;
