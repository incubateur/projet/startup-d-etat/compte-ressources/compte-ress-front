import { Link, Outlet, useChildMatches } from '@tanstack/react-router';
import { useTranslation } from 'react-i18next';

import { ADMIN_ROLES } from 'shared/enums/userEnums';

import useAuth from 'shared/hooks/useAuth';

import { getUserNavItems, hasRole } from 'shared/services/userServices';

const AccountPage = () => {
  const { t } = useTranslation('account');
  const { userRoles } = useAuth();
  const userNavItems = getUserNavItems(userRoles);
  const childMatches = useChildMatches();

  const isAdmin = hasRole(userRoles, [...ADMIN_ROLES]);

  if (childMatches.length) return <Outlet />; // redirect to child if there is a child

  return (
    <div className="p-8 pt-3">
      <h1 className="mb-8 text-2xl font-bold">{t('userAccount.title')}</h1>
      <div className="flex flex-col gap-8">
        {userNavItems.map((navItem) => (
          <Link
            to={navItem.to}
            search={{ page: 1 }}
            key={navItem.key}
            className="rounded-sm border border-gray-200 p-8"
          >
            <h2 className="text-xl font-bold">
              {t(`menu.${isAdmin ? 'admin' : 'teacher'}.${navItem.key}`, { ns: 'account' })}
            </h2>{' '}
            <p className="font-light text-gray-600">
              lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            </p>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default AccountPage;
