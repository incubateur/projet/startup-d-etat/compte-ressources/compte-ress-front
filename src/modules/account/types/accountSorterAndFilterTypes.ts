import type {
  adminResourcesSorters,
  ALL,
  filterUserResources,
} from 'modules/account/enums/accountSorterAndFilterEnums';

import type {
  AdminOrdersStatus,
  AdminRequestsStatus,
} from 'modules/account/types/accountOrderStatusTypes';

export type AdminRequestsFilter = AdminRequestsStatus | typeof ALL;
export type AdminOrdersFilter = AdminOrdersStatus | typeof ALL;

export type SortingOrder = (typeof adminResourcesSorters)[number];

export type UserResourcesFilter = (typeof filterUserResources)[number];
