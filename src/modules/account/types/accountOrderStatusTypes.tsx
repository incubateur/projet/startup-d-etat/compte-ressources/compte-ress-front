import type {
  adminOrdersStatusEnums,
  adminRequestsStatusEnums,
  teacherRequestsStatusEnums,
} from 'modules/account/enums/accountOrderStatusEnums';

import type { CartItem } from 'modules/cart/types/cartTypes';

export type TeacherRequestsStatus = (typeof teacherRequestsStatusEnums)[number];
export type AdminOrdersStatus = (typeof adminOrdersStatusEnums)[number];
export type AdminRequestsStatus = (typeof adminRequestsStatusEnums)[number];

// TeacherRequests
export type Request = CartItem & {};

// AdminRequests

export type AdminRequest = {
  orders: Array<CartItem>;
  status: AdminRequestsStatus;
  ark: string;
};

// AdminOrders
