import type { z } from 'zod';

import type {
  transactionSchema,
  walletBalanceSchema,
  walletSchema,
} from 'modules/account/store/server/wallet/walletSchemas';

export type Transaction = z.infer<typeof transactionSchema>;
export type Transactions = Array<Transaction & { index: number }>;

export type Wallet = z.infer<typeof walletSchema>;
export type WalletBalance = z.infer<typeof walletBalanceSchema>;
