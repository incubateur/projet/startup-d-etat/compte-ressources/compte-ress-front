import { Button } from '@codegouvfr/react-dsfr/Button';
import { Select } from '@codegouvfr/react-dsfr/Select';
import { Link } from '@tanstack/react-router';
import { useTranslation } from 'react-i18next';

import { filterUserResources } from 'modules/account/enums/accountSorterAndFilterEnums';
import { sortingOrders } from 'modules/catalog/enums/sortingEnums';
import { PLACEHOLDER_CART_ITEM } from 'shared/enums/orderEnums';

import type { UserResourcesFilter } from 'modules/account/types/accountSorterAndFilterTypes';
import type { OrderItem } from 'shared/types/orderTypes';

import useUserResources from 'modules/account/hooks/useUserResources';

import { findOrderTitle } from 'modules/account/services/adminServices';

import NoResourceFound from 'modules/account/components/NoResourceFound';
import CartItemDescription from 'modules/cart/components/CartItemDescription';
import { Badge, Card, Pagination } from 'shared/components/dsfr';
import { InnerLoader } from 'shared/components/molecules/InnerLoader';

type UserResourcesProps = {
  resources: Array<OrderItem> | undefined;
  isLoading: boolean;
  isAdmin: boolean;
  hasNoResources: boolean;
};

const UserResources = ({ resources, isLoading, isAdmin, hasNoResources }: UserResourcesProps) => {
  const { t } = useTranslation('account');

  if (isLoading) return <InnerLoader />;
  if (hasNoResources)
    return <NoResourceFound pageType={isAdmin ? 'adminResources' : 'teacherResources'} />;
  if (!resources?.length)
    return <NoResourceFound label={t('noResourceFound.userSelectionResource')} />;

  return resources?.map((resource) => (
    <Card
      badge={
        resource.info.resourceTypes?.length ? <Badge>{resource.info.resourceTypes[0]}</Badge> : null
      }
      key={resource.id}
      background
      className="py-5"
      classes={{
        desc: 'm-0',
      }}
      border={false}
      footer={null}
      desc={
        <CartItemDescription
          editor={resource.info.editor}
          isEditable={false}
          date={resource.info.date as string}
          credits={resource?.price}
          educationalLevels={resource.info.educationalLevels}
        />
      }
      horizontal
      imageAlt={findOrderTitle(resource)}
      imageUrl={resource.info.vignette || PLACEHOLDER_CART_ITEM}
      size="small"
      title={
        <Link to="/catalogue/$ressourceId" params={{ ressourceId: resource?.ark }}>
          {resource.info.title}
        </Link>
      }
      titleAs="h3"
    />
  ));
};

const UserResourcesPage = () => {
  const { t } = useTranslation('account');
  const { userResources, isLoadingOrders, onFilterChange, pagination, isAdmin } =
    useUserResources();

  return (
    <>
      <div className="flex items-center justify-between py-3 pl-0">
        <h1 className="text-2xl font-bold">
          {t(`userResources.title.${isAdmin ? 'admin' : 'teacher'}`)}
        </h1>
        <div className="flex items-center gap-2">
          {t('userResources.display')}
          <Select
            className="mt-0"
            label={null}
            nativeSelectProps={{
              name: 'sortingOrder',
              defaultValue: sortingOrders[0],
              onChange: (event) => onFilterChange(event.target.value as UserResourcesFilter),
            }}
          >
            {filterUserResources.map((filerType) => (
              <option key={filerType} value={filerType}>
                {t(`userResources.${filerType}`)}
              </option>
            ))}
          </Select>
        </div>
      </div>
      <div className="flex min-h-[300px] flex-col gap-8">
        <UserResources
          resources={userResources}
          isLoading={isLoadingOrders}
          isAdmin={isAdmin}
          hasNoResources={pagination.total === 0}
        />
        <Pagination
          currentPage={pagination.currentPage}
          pageSize={pagination.pageSize}
          totalItems={pagination.total || 0}
        />
      </div>
      <Button
        className="float-right mb-16"
        priority="tertiary"
        onClick={() => window.scrollTo({ top: 0 })}
        iconId="fr-icon-arrow-up-line"
      >
        {t('scrollToTop', { ns: 'catalog' })}
      </Button>
    </>
  );
};

export default UserResourcesPage;
