import { Link } from '@tanstack/react-router';
import dayjs from 'dayjs';
import { useTranslation } from 'react-i18next';

import {
  creditsPerStatusConfig,
  creditsPerStatusData,
  schoolCreditsPerStatusData,
} from 'modules/account/enums/creditsDataEnums';
import { DEBIT } from 'modules/account/enums/walletTransactionsEnums';

import type { Transaction } from 'modules/account/types/walletTypes';

import useUserCredits from 'modules/account/hooks/useUserCredits';

import CreditsPieChart from 'modules/account/components/CreditsPieCharts';
import { Button, Pagination } from 'shared/components/dsfr';

const TransactionDetails = ({ transaction }: { transaction: Transaction }) => {
  const { t } = useTranslation('account');

  const isDebit = transaction.type === DEBIT;

  return (
    <tr className="border-b bg-white">
      <td className="px-6 py-4">{dayjs(transaction.createdAt).format('DD/MM/YYYY')}</td>
      <td className="px-6 py-4">
        {isDebit ? (
          <Link to="/catalogue/$ressourceId" params={{ ressourceId: transaction.cause }}>
            <div className="max-w-[500px] truncate">
              {transaction.info?.title || transaction.cause}
            </div>
          </Link>
        ) : (
          t(`userCredits.${transaction.type}`)
        )}
      </td>
      <td className="px-6 py-4">
        {!!transaction.amount && <span className="mr-1">{isDebit ? '-' : '+'}</span>}
        <span className="mr-1">{transaction.amount ? transaction.amount : null}</span>
        <span>{t('userCredits.credits', { count: transaction.amount })}</span>
      </td>
      <td className="px-6 py-4">
        {transaction.total} {t('userCredits.credits', { count: transaction.total })}
      </td>
    </tr>
  );
};

const UserCredits = () => {
  const { t } = useTranslation('account');

  const { total, transactions, balance, addCredit, pagination, balanceFetched, isAdmin, page } =
    useUserCredits();

  if (!transactions) return null;

  return (
    <>
      <div className="flex items-center justify-between py-3 pl-0">
        <h1 className="text-2xl font-bold">
          {t(`userCredits.title.${isAdmin ? 'admin' : 'teacher'}`)}
        </h1>
        <div>
          <Button priority="tertiary" onClick={addCredit}>
            {t(`userCredits.${isAdmin ? 'assignCredit' : 'addCredit'}`)}
          </Button>
          {isAdmin && <Button priority="tertiary">{t('userCredits.exportData')}</Button>}
        </div>
      </div>
      {isAdmin && (
        <div className="rounded-md border border-gray-200 p-8">
          <h2 className="pb-2 text-xl font-bold text-gray-700">{t('userCredits.charts.title')}</h2>
          <CreditsPieChart
            config={creditsPerStatusConfig}
            dataUser={creditsPerStatusData}
            dataSchool={schoolCreditsPerStatusData}
            nameKey="credits"
            labelKey="status"
          />
        </div>
      )}
      {balanceFetched ? (
        <div className="mt-8 rounded-md border border-gray-200 p-8">
          <h2 className="pb-2 text-xl font-bold text-gray-700">{t('userCredits.balance')}</h2>
          <p>
            <span className="pr-1 font-bold text-gray-700">{balance || null}</span>
            {balance ? t('userCredits.credits', { count: balance }) : t('userCredits.noCredit')}
          </p>
        </div>
      ) : null}
      <div className="my-8">
        <h2 className="pb-4 text-xl font-bold text-gray-700">{t('userCredits.history')}</h2>
        <table className="w-full text-left text-gray-500">
          <thead className="w-full rounded-sm border border-gray-200 bg-gray-50 p-4 text-xs font-bold uppercase text-gray-700">
            <tr>
              <th scope="col" className="px-6 py-3">
                {t('userCredits.table.date')}
              </th>
              <th scope="col" className="px-6 py-3">
                {t('userCredits.table.type')}
              </th>
              <th scope="col" className="px-6 py-3">
                {t('userCredits.table.amount')}
              </th>
              <th scope="col" className="px-6 py-3">
                {t('userCredits.table.balance')}
              </th>
            </tr>
          </thead>
          <tbody>
            {transactions.map((transaction) => (
              <TransactionDetails key={transaction.index} transaction={transaction} />
            ))}
          </tbody>
        </table>
        <Pagination
          currentPage={page}
          pageSize={pagination.pageSize}
          totalItems={total || 0}
          onChange={pagination.onChange}
        />
      </div>
    </>
  );
};
export default UserCredits;
