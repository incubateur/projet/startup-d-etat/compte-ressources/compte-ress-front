import { useTranslation } from 'react-i18next';

type NoResourceFoundProps = {
  pageType?: 'adminResources' | 'teacherResources' | 'userOrders' | 'userCredits' | 'userRequests';
  label?: string;
};

const NoResourceFound = ({ pageType, label }: NoResourceFoundProps) => {
  const { t } = useTranslation('account');

  return (
    <div className="w-full pt-8 font-bold">
      {label || (pageType && t(`noResourceFound.${pageType}`))}
    </div>
  );
};

export default NoResourceFound;
