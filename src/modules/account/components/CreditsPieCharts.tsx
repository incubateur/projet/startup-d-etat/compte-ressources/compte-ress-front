import { useTranslation } from 'react-i18next';
import { Pie, PieChart } from 'recharts';
import type { Payload } from 'recharts/types/component/DefaultLegendContent';

import {
  type ChartConfig,
  ChartContainer,
  ChartLegend,
  ChartLegendContent,
  ChartTooltip,
  ChartTooltipContent,
} from 'shared/components/molecules/Chart';

type CreditsPieChartProps = {
  config: ChartConfig;
  dataUser: Array<any>;
  dataSchool: Array<any>;
  nameKey: string;
  labelKey: string;
};

const uniquePayload = (payload: Array<Payload> | undefined) => {
  if (!payload) return [];
  const uniqueSet = new Set<number>();
  return payload.filter((dataItem) => {
    if (uniqueSet.has(dataItem.value)) {
      return false;
    }
    uniqueSet.add(dataItem.value);
    return true;
  });
};

const CreditsPieChart = ({
  config,
  dataUser,
  dataSchool,
  nameKey,
  labelKey,
}: CreditsPieChartProps) => {
  const { t } = useTranslation('account');
  return (
    <div className="overflow-auto">
      <ChartContainer config={config} className="flex aspect-square max-h-[300px] w-[600px]">
        <PieChart>
          <ChartTooltip
            content={<ChartTooltipContent nameKey={nameKey} hideLabel className="bg-white" />}
          />
          <text
            x="150"
            y="0"
            dy={+12}
            className="text-sm font-bold text-gray-600"
            width={200}
            textAnchor="middle"
          >
            {t('userCredits.charts.individualChart')}
          </text>
          <text
            x="400"
            y="0"
            dy={+12}
            className="text-sm font-bold text-gray-600"
            width={200}
            textAnchor="middle"
          >
            {t('userCredits.charts.schoolChart')}
          </text>
          <Pie data={dataUser} dataKey={nameKey} labelLine={false} nameKey="value" cx={150} />
          <Pie data={dataSchool} dataKey={nameKey} labelLine={false} nameKey="value" cx={400} />
          <ChartLegend
            // eslint-disable-next-line react/no-unstable-nested-components
            content={({ payload }) => (
              <ChartLegendContent nameKey={labelKey} payload={uniquePayload(payload)} />
            )}
            className="-translate-y-2 flex-wrap gap-2 [&>*]:basis-1/4 [&>*]:justify-center"
          />
        </PieChart>
      </ChartContainer>
    </div>
  );
};

export default CreditsPieChart;
