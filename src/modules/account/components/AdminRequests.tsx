import { useTranslation } from 'react-i18next';

import { adminRequestsStatusEnums } from 'modules/account/enums/accountOrderStatusEnums';
import { ALL, adminResourcesSorters } from 'modules/account/enums/accountSorterAndFilterEnums';
import { DATE_DESC } from 'modules/catalog/enums/sortingEnums';

import type {
  AdminRequest,
  AdminRequestsStatus,
} from 'modules/account/types/accountOrderStatusTypes';
import type {
  AdminRequestsFilter,
  SortingOrder,
} from 'modules/account/types/accountSorterAndFilterTypes';
import type { OrderStatus } from 'shared/types/orderTypes';

import useAdminRequests from 'modules/account/hooks/useAdminRequests';

import NoResourceFound from 'modules/account/components/NoResourceFound';
import { Button, Pagination, Select } from 'shared/components/dsfr';
import Card from 'shared/components/layout/Card';
import { InnerLoader } from 'shared/components/molecules/InnerLoader';

const adminRequestsFilters = [ALL, ...adminRequestsStatusEnums] as const;

type AdminRequestsResourcesProps = {
  adminRequests: Array<AdminRequest> | undefined;
  isLoading: boolean;
  checkedItems: { [id: string]: boolean };
  checkedStatus: AdminRequestsStatus | undefined;
  onSelectCard: (ark: string, status?: OrderStatus) => void;
};

const AdminRequestsItems = ({
  adminRequests,
  isLoading,
  checkedItems,
  checkedStatus,
  onSelectCard,
}: AdminRequestsResourcesProps) => {
  if (isLoading) return <InnerLoader />;
  if (!adminRequests?.length) return <NoResourceFound pageType="userRequests" />;

  return adminRequests.map(({ orders, status, ark }) => (
    <Card
      key={orders[0].id}
      orders={orders}
      onSelectCard={onSelectCard}
      isChecked={!!checkedItems[orders[0].id]}
      disabled={checkedStatus && status !== checkedStatus}
      value={ark}
    />
  ));
};

const AdminRequests = () => {
  const { t } = useTranslation('account');

  const {
    adminRequests,
    checkedItems,
    areAllCardsSelected,
    modal,
    modalType,
    isLoading,
    checkedStatus,
    isDirector,
    onFilteringChange,
    onSortingChange,
    onSelectCard,
    onSelectAllCard,
    onModalOpen,
    onModalConfirmClick,
    pagination,
  } = useAdminRequests();

  return (
    <>
      <div className="flex items-center justify-between py-3 pl-0">
        <h1 className="text-2xl font-bold">{t('admin.requests.title')}</h1>
      </div>
      <div className="mb-5 flex items-center justify-between border-none">
        <div className="flex gap-2">
          <Button className="w-[200px]" priority="tertiary" onClick={onSelectAllCard}>
            <span className="w-full text-center">
              {t(`${areAllCardsSelected ? 'admin.unselectAll' : 'admin.selectAll'}`)}
            </span>
          </Button>
          <Button
            disabled={!areAllCardsSelected || checkedStatus !== 'submitted' || !isDirector}
            onClick={() => onModalOpen('validate')}
          >
            {t(`admin.requests.validate.title`)}
          </Button>
          <Button
            disabled={!areAllCardsSelected || checkedStatus !== 'validated' || !isDirector}
            onClick={() => onModalOpen('order')}
          >
            {t(`admin.requests.order.title`)}
          </Button>
        </div>
        <div>
          <div className="flex gap-4">
            <Select
              className="!my-0"
              label={null}
              nativeSelectProps={{
                name: 'filterResources',
                defaultValue: adminRequestsFilters[0],
                onChange: (event) => onFilteringChange(event.target.value as AdminRequestsFilter),
              }}
            >
              {adminRequestsFilters.map((filter) => (
                <option key={filter} value={filter}>
                  {t(`admin.requests.filter.${filter}`)}
                </option>
              ))}
            </Select>
            <Select
              className="!my-0"
              label={null}
              nativeSelectProps={{
                name: 'sortingResources',
                defaultValue: DATE_DESC,
                onChange: (event) => onSortingChange(event.target.value as SortingOrder),
              }}
            >
              {adminResourcesSorters.map((sorter) => (
                <option key={sorter} value={sorter}>
                  {t(`admin.sort.${sorter}`)}
                </option>
              ))}
            </Select>
          </div>
        </div>
      </div>
      <AdminRequestsItems
        adminRequests={adminRequests}
        isLoading={isLoading}
        onSelectCard={onSelectCard}
        checkedItems={checkedItems}
        checkedStatus={checkedStatus}
      />
      <Pagination
        currentPage={pagination.currentPage}
        pageSize={pagination.pageSize}
        totalItems={pagination.total}
      />
      <modal.Component
        buttons={[
          {
            doClosesModal: true,
            children: t('admin.cancel'),
          },
          {
            children: t(`admin.requests.${modalType}.title`),
            onClick: onModalConfirmClick,
          },
        ]}
        title={t(`admin.requests.${modalType}.title`)}
      >
        <span>{t(`admin.requests.${modalType}.body`)}</span>
      </modal.Component>
    </>
  );
};

export default AdminRequests;
