import { useTranslation } from 'react-i18next';

import { teacherRequestsStatusEnums } from 'modules/account/enums/accountOrderStatusEnums';

import type { TeacherRequestsStatus } from 'modules/account/types/accountOrderStatusTypes';
import type { OrderItem } from 'shared/types/orderTypes';

import { defineColorFromStatusRequest } from 'modules/account/services/accountService';

import { Tag } from 'shared/components/dsfr';

import { formatDate } from 'shared/utils/dateUtils';

type UserOrderItemsProps = {
  orders: OrderItem[];
};
const UserOrderItems = ({ orders }: UserOrderItemsProps) => {
  const { t } = useTranslation('account');
  const { status } = orders[0];

  if (!teacherRequestsStatusEnums.some((val) => val === status)) return null;

  return (
    <>
      <div className="max-h-[100px] overflow-auto">
        {orders.map((order) => (
          <div key={order.id} className="my-1 text-gray-500">
            <span className="mr-1 font-bold">{t('requestsPage.sent_at')}</span>
            <span className="mr-1 font-bold">{formatDate(order.modifiedAt)}</span>
            <span className="mr-1 font-bold">{t('requestsPage.by')}</span>
            <div className="flex">
              <span className="fr-icon-user-line mr-1" aria-hidden="true" />
              <span>
                {order.info.user.firstname} {order.info.user.lastname}
              </span>
            </div>
          </div>
        ))}
      </div>
      <Tag
        className={`mt-2 rounded-sm border ${defineColorFromStatusRequest(status, 'border')} ${defineColorFromStatusRequest(status)}`}
      >
        {t(`requestsPage.status.${status as TeacherRequestsStatus}`)}
      </Tag>
    </>
  );
};

export default UserOrderItems;
