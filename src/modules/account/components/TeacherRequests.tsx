import Badge from '@codegouvfr/react-dsfr/Badge';
import Card from '@codegouvfr/react-dsfr/Card';
import { Select } from '@codegouvfr/react-dsfr/Select';
import { Link } from '@tanstack/react-router';
import { useTranslation } from 'react-i18next';

import { ALL_YEARS, yearFilters } from 'modules/account/enums/accountSorterAndFilterEnums';
import { PLACEHOLDER_CART_ITEM } from 'shared/enums/orderEnums';

import type { TeacherRequestsStatus } from 'modules/account/types/accountOrderStatusTypes';
import type { OrderItem } from 'shared/types/orderTypes';

import useTeacherRequests from 'modules/account/hooks/useTeacherRequests';

import { findOrderTitle } from 'modules/account/services/adminServices';

import NoResourceFound from 'modules/account/components/NoResourceFound';
import RequestStatus from 'modules/account/components/RequestedResourceStatus';
import CartItemDescription from 'modules/cart/components/CartItemDescription';
import { Pagination } from 'shared/components/dsfr';
import { InnerLoader } from 'shared/components/molecules/InnerLoader';

type TeacherRequestsProps = {
  teacherRequests: Array<OrderItem> | undefined;
  isLoading: boolean;
  hasNoRequest: boolean;
};

const TeacherRequests = ({ teacherRequests, isLoading, hasNoRequest }: TeacherRequestsProps) => {
  const { t } = useTranslation('account');

  if (isLoading) return <InnerLoader />;
  if (hasNoRequest) return <NoResourceFound pageType="userRequests" />;
  if (!teacherRequests?.length)
    return <NoResourceFound label={t('noResourceFound.teacherRequests')} />;

  return teacherRequests?.map((order) => (
    <Card
      badge={order.info.resourceTypes?.length ? <Badge>{order.info.resourceTypes[0]}</Badge> : null}
      key={order.id}
      background
      className="py-5"
      classes={{
        desc: 'm-0',
      }}
      border={false}
      desc={
        <CartItemDescription
          editor={order.info.editor}
          isEditable={false}
          date={order.info.date}
          credits={order.price}
          educationalLevels={order.info.educationalLevels}
        />
      }
      horizontal
      imageAlt={findOrderTitle(order)}
      imageUrl={order.info.vignette || PLACEHOLDER_CART_ITEM}
      size="small"
      title={
        <Link to="/catalogue/$ressourceId" params={{ ressourceId: order?.ark }}>
          {order.info.title}
        </Link>
      }
      footer={
        <RequestStatus
          date={order.info.date as string}
          modifiedAt={order.modifiedAt}
          status={order.status as TeacherRequestsStatus}
        />
      }
      titleAs="h3"
    />
  ));
};

const TeacherRequestsPage = () => {
  const { pagination, isPending, teacherRequests, setFilterByYear } = useTeacherRequests();
  const onFilteringByYear = (filterDate: string) => {
    setFilterByYear(filterDate);
  };
  const { t } = useTranslation('account');

  return (
    <div className="divide-y-5 divide-y">
      <div className="flex items-center justify-between">
        <h1 className="mt-5 text-2xl font-bold">{t('menu.teacher.requests')}</h1>
        <div className="mb-5 flex items-center border-none">
          <h3>{t('requestsPage.display')}</h3>
          <Select
            className="ml-8 mt-0 [&>*]:!mt-0"
            label={null}
            nativeSelectProps={{
              name: 'filterByYear',
              defaultValue: 'all',
              onChange: ({ target }) => onFilteringByYear(target.value),
            }}
          >
            <option value={ALL_YEARS}>{t('requestsPage.display_default_option')}</option>

            {yearFilters.map((yearFilter) => (
              <option key={yearFilter} value={yearFilter}>
                {yearFilter}
              </option>
            ))}
          </Select>
        </div>
      </div>
      <TeacherRequests
        teacherRequests={teacherRequests}
        isLoading={isPending}
        hasNoRequest={teacherRequests?.length === 0}
      />
      <Pagination
        currentPage={pagination.currentPage}
        pageSize={pagination.pageSize}
        totalItems={pagination.total}
      />
    </div>
  );
};
export default TeacherRequestsPage;
