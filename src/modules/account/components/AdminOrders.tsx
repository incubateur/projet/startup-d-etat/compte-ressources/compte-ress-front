import { useTranslation } from 'react-i18next';

import { adminOrdersStatusEnums } from 'modules/account/enums/accountOrderStatusEnums';
import {
  ALL,
  DATE_DESC,
  adminResourcesSorters,
} from 'modules/account/enums/accountSorterAndFilterEnums';

import type {
  AdminOrdersFilter,
  SortingOrder,
} from 'modules/account/types/accountSorterAndFilterTypes';
import type { Order } from 'shared/types/orderTypes';

import useAdminOrders from 'modules/account/hooks/useAdminOrders';

import NoResourceFound from 'modules/account/components/NoResourceFound';
import { Button, Pagination, Select } from 'shared/components/dsfr';
import Card from 'shared/components/layout/Card';
import { InnerLoader } from 'shared/components/molecules/InnerLoader';

const adminOrdersFilters = [ALL, ...adminOrdersStatusEnums] as const;

type AdminOrderItemsProps = {
  adminOrders: Order | undefined;
  isLoading: boolean;
  checkedItems: Array<string>;
  onSelectCard: ((value: string) => void) | undefined;
};

const AdminOrderItems = ({
  adminOrders,
  isLoading,
  checkedItems,
  onSelectCard,
}: AdminOrderItemsProps) => {
  if (isLoading) return <InnerLoader />;
  if (!adminOrders?.length) return <NoResourceFound pageType="userRequests" />;

  return adminOrders.map((order) => (
    <Card
      key={order.id}
      orders={[order]}
      onSelectCard={onSelectCard}
      isChecked={checkedItems.some((item) => item === order.id)}
      disabled={order.status === 'assigned'}
      value={order.id}
    />
  ));
};

const AdminOrders = () => {
  const { t } = useTranslation('account');

  const {
    adminOrders,
    isPending,
    modal,
    checkedItems,
    areAllCardsSelected,
    isAssignmentManager,
    onFilteringChange,
    onSortingChange,
    onSelectAllCard,
    onSetAssignedStatusOrder,
    onModalConfirmClick,
    onSelectCard,
    pagination,
  } = useAdminOrders();

  return (
    <>
      <div className="flex items-center justify-between py-3 pl-0">
        <h1 className="text-2xl font-bold">{t('admin.orders.title')}</h1>
      </div>
      <div className="mb-5 flex items-center justify-between border-none">
        <div className="grid grid-cols-2 divide-x">
          <Button
            className="mr-5 w-[200px]"
            priority="tertiary"
            onClick={onSelectAllCard}
            disabled={!isAssignmentManager}
          >
            <span className="w-full text-center">
              {t(`${areAllCardsSelected ? 'admin.unselectAll' : 'admin.selectAll'}`)}
            </span>
          </Button>
          <Button
            disabled={checkedItems.length === 0 || !isAssignmentManager}
            className="mr-5"
            onClick={onSetAssignedStatusOrder}
          >
            {t('admin.orders.markAsAssigned')}
          </Button>
        </div>

        <div>
          <div className="flex gap-4">
            <Select
              className="!my-0"
              label={null}
              nativeSelectProps={{
                name: 'filterResources',
                defaultValue: adminOrdersFilters[0],
                onChange: (event) => onFilteringChange(event.target.value as AdminOrdersFilter),
              }}
            >
              {adminOrdersFilters.map((filter) => (
                <option key={filter} value={filter}>
                  {t(`admin.orders.filter.${filter}`)}
                </option>
              ))}
            </Select>
            <Select
              className="!my-0"
              label={null}
              nativeSelectProps={{
                name: 'sortingResources',
                defaultValue: DATE_DESC,
                onChange: (event) => onSortingChange(event.target.value as SortingOrder),
              }}
            >
              {adminResourcesSorters.map((sorter) => (
                <option key={sorter} value={sorter}>
                  {t(`admin.sort.${sorter}`)}
                </option>
              ))}
            </Select>
          </div>
        </div>
      </div>
      <AdminOrderItems
        adminOrders={adminOrders}
        isLoading={isPending}
        checkedItems={checkedItems}
        onSelectCard={isAssignmentManager ? onSelectCard : undefined}
      />
      <Pagination
        currentPage={pagination.currentPage}
        pageSize={pagination.pageSize}
        totalItems={pagination.total}
      />
      <modal.Component
        buttons={[
          {
            doClosesModal: true,
            children: t('admin.cancel'),
          },
          {
            children: t('admin.validate'),
            onClick: onModalConfirmClick,
          },
        ]}
        title={t('admin.orders.markAsAssigned')}
      >
        <span>{t('admin.orders.markAsAssignedModalBody')}</span>
      </modal.Component>
    </>
  );
};

export default AdminOrders;
