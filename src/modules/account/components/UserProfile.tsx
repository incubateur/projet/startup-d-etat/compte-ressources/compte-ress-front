import { useTranslation } from 'react-i18next';

import { ADMIN_ROLES } from 'shared/enums/userEnums';

import useAuth from 'shared/hooks/useAuth';

import { hasRole } from 'shared/services/userServices';

const UserProfileItem = ({ title, value }: { title: string; value?: string }) => {
  if (!value) return null;
  return (
    <div>
      <p className="text-sm font-bold text-gray-600">{title}</p>
      <p id={value} className="font-light text-gray-800">
        {value}
      </p>
    </div>
  );
};

const UserProfile = () => {
  const { t } = useTranslation('account');
  const { userInfo, userRoles } = useAuth();

  const isAdmin = hasRole(userRoles, [...ADMIN_ROLES]);

  return (
    <div className="p-8 pt-3">
      <h1 className="mb-8 text-2xl font-bold">{t('userProfile.title')}</h1>
      <div className="flex flex-col gap-8">
        <div className="flex flex-col gap-4 rounded-sm border border-gray-200 p-8">
          <h2 className="pb-2 text-xl font-bold">{t('userProfile.informations')}</h2>
          <UserProfileItem title={t('userProfile.firstName')} value={userInfo?.given_name} />
          <UserProfileItem title={t('userProfile.lastName')} value={userInfo?.family_name} />
          {!isAdmin && <UserProfileItem title={t('userProfile.subject')} value="Français" />}
          <UserProfileItem title={t('userProfile.email')} value={userInfo?.email} />
        </div>
        <div className="flex flex-col gap-4 rounded-sm border border-gray-200 p-8">
          <h2 className="pb-2 text-xl font-bold">{t('userProfile.institution')}</h2>
          <p>Collège Antoine de Saint-Exupéry</p>
          <UserProfileItem title={t('userProfile.code')} value="0930593P" />
        </div>
        <div className="flex flex-col gap-4 rounded-sm border border-gray-200 p-8">
          <h2 className="pb-2 text-xl font-bold">{t('userProfile.contacts')}</h2>
          <UserProfileItem
            title={t('userProfile.headOfSchool')}
            value="prenom.nom@education-gouv.fr"
          />
          <UserProfileItem
            title={t('userProfile.digitalReferent')}
            value="prenom.nom@education-gouv.fr"
          />
          <UserProfileItem
            title={t('userProfile.assignmentManager')}
            value="prenom.nom@education-gouv.fr"
          />
        </div>
      </div>
    </div>
  );
};

export default UserProfile;
