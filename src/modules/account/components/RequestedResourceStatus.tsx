import { Tooltip } from '@mui/material';
import { Trans, useTranslation } from 'react-i18next';

import { DATE_FRENCH_FORMAT_SHORT } from 'shared/enums/dateEnums';

import type { TeacherRequestsStatus } from 'modules/account/types/accountOrderStatusTypes';

import { defineColorFromStatusRequest } from 'modules/account/services/accountService';

import { fr, Tag } from 'shared/components/dsfr';

import { formatDate } from 'shared/utils/dateUtils';

type RequestStatusProps = {
  status: TeacherRequestsStatus;
  date: string;
  modifiedAt: string;
};

const RequestStatus = ({ status, date, modifiedAt }: RequestStatusProps) => {
  const { t } = useTranslation('account');

  return (
    <div className="flex items-center">
      <span className="mr-1">{t('requestsPage.sent_at')}</span>
      <span className="mr-1">{formatDate(date, DATE_FRENCH_FORMAT_SHORT)}</span>
      <Tag
        className={`rounded-sm border ${defineColorFromStatusRequest(status, 'border')} ${defineColorFromStatusRequest(status)}`}
      >
        {t(`requestsPage.status.${status}`)}
      </Tag>
      <span className="mx-1">{t('requestsPage.at')}</span>
      <span className="mx-1">{formatDate(modifiedAt, DATE_FRENCH_FORMAT_SHORT)}</span>

      <Tooltip
        placement="top"
        arrow
        title={
          <Trans
            i18nKey={`requestsPage.status.description.${status}`}
            components={{ b: <span className="font-bold" /> }}
            ns="account"
          />
        }
      >
        <span className={fr.cx('fr-icon-question-line')} aria-hidden />
      </Tooltip>
    </div>
  );
};

export default RequestStatus;
