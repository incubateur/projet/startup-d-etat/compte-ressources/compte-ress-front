import {
  ALL,
  DATE_ASC,
  LICENCE_ASC,
  LICENCE_DESC,
} from 'modules/account/enums/accountSorterAndFilterEnums';

import type {
  AdminRequest,
  AdminRequestsStatus,
  TeacherRequestsStatus,
} from 'modules/account/types/accountOrderStatusTypes';
import type { SortingOrder } from 'modules/account/types/accountSorterAndFilterTypes';
import type { CartItem } from 'modules/cart/types/cartTypes';
import type { Order, OrderItem, OrderStatus } from 'shared/types/orderTypes';

export const filterAdminRequestsByStatus = (
  status: 'submitted' | 'validated',
  requests: CartItem[],
  checkedItems: { [id: string]: boolean }
): Array<string> =>
  requests
    .filter((request) => request.status === status)
    .filter((request) => checkedItems[request.id])
    .map((request) => request.id);

export const filterAndSortAdminRequests = <T extends OrderStatus | typeof ALL>(
  adminRequestsData: Order | undefined | null,
  selectedFilter: T,
  selectedSorter: SortingOrder,
  dateStatus: TeacherRequestsStatus
): Order =>
  adminRequestsData
    ?.filter((request) => {
      if (selectedFilter === ALL) return true;
      return request.status === selectedFilter;
    })
    .sort((a, b) => {
      const dateA = a.info.statusHistory?.[dateStatus] || a.modifiedAt;
      const dateB = b.info.statusHistory?.[dateStatus] || b.modifiedAt;
      if (selectedSorter === DATE_ASC) return dateA.localeCompare(dateB);
      if (selectedSorter === LICENCE_ASC) return a.licenseType.localeCompare(b.licenseType);
      if (selectedSorter === LICENCE_DESC) return b.licenseType.localeCompare(a.licenseType);
      return dateB.localeCompare(dateA);
    }) ?? [];

export const groupAdminRequests = (adminRequests: Order) =>
  adminRequests.reduce((acc, item) => {
    const { ark } = item;
    const adminRequest = acc.find(
      (request) => request.ark === ark && request.status === item.status
    );
    if (adminRequest) {
      adminRequest.orders.push(item);
      return acc;
    }
    return [
      ...acc,
      {
        ark,
        orders: [item],
        status: item.status as AdminRequestsStatus,
      },
    ];
  }, [] as Array<AdminRequest>);

export const findSelectedStatus = (
  checkedItems: { [id: string]: boolean },
  groupedAdminRequests: Array<AdminRequest>
) => {
  const checkedIds = Object.keys(checkedItems).filter((id) => checkedItems[id]);

  const checkedStatus = groupedAdminRequests.find(
    ({ orders }) => orders[0].id === checkedIds[0]
  )?.status;

  return checkedStatus;
};

export const findAllSelectedOrders = (
  selectedOrders: Record<string, boolean>,
  adminRequests: Order
) => {
  const allChecked = Object.values(selectedOrders).every(Boolean);
  const allUnchecked = Object.values(selectedOrders).every((i) => !i);
  if (allUnchecked) {
    return adminRequests.reduce((acc, item) => ({ ...acc, [item.id]: true }), {});
  }
  return adminRequests.reduce((acc, item) => ({ ...acc, [item.id]: !allChecked }), {});
};

export const findOrderTitle = (order: OrderItem) => {
  const title = typeof order.info?.title === 'string' ? order.info.title : order.info.title[0];
  return title;
};
