import { ASSIGNED, ORDERED, SUBMITTED, VALIDATED } from 'shared/enums/orderEnums';

export const defineColorFromStatusRequest = (status: string, props = 'bg') => {
  switch (status) {
    case ASSIGNED:
      if (props === 'border') {
        return 'border-amber-400';
      }
      return `bg-amber-100`;
    case VALIDATED:
      if (props === 'border') {
        return 'border-green-400';
      }
      return `bg-green-100`;
    case ORDERED:
      if (props === 'border') {
        return 'border-amber-400';
      }
      return `bg-amber-100`;
    case SUBMITTED:
      if (props === 'border') {
        return 'border-gray-400';
      }
      return `bg-gray-100`;
    default:
      if (props === 'border') {
        return 'border-none';
      }
      return 'bg-none';
  }
};
