import { keepPreviousData, useQuery } from '@tanstack/react-query';

import {
  fetchWalletApi,
  fetchWalletBalanceApi,
} from 'modules/account/store/server/wallet/fetchWalletApi';

export const useWalletQuery = (token: string | undefined, page: number) =>
  useQuery({
    queryKey: ['wallet', page],
    queryFn: () => fetchWalletApi(token, page),
    enabled: !!token,
    placeholderData: keepPreviousData,
  });

export const useWalletBalanceQuery = (token: string | undefined) =>
  useQuery({
    queryKey: ['walletBalance'],
    queryFn: () => fetchWalletBalanceApi(token),
    enabled: !!token,
  });
