import { z } from 'zod';

import { CREDIT, DEBIT } from 'modules/account/enums/walletTransactionsEnums';

import { metaDataSchema } from 'shared/store/server/metaDataSchema';

export const transactionSchema = z.object({
  amount: z.number(),
  cause: z.string(),
  type: z.union([z.literal(CREDIT), z.literal(DEBIT)]),
  createdAt: z.string(),
  total: z.number(),
  info: z.nullable(
    z.object({
      title: z.string(),
    })
  ),
});

export const walletSchema = z.object({
  data: z.array(transactionSchema),
  meta: metaDataSchema,
});

export const walletBalanceSchema = z.object({
  balance: z.number(),
});
