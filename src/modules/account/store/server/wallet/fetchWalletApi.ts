import type { Transactions, WalletBalance } from 'modules/account/types/walletTypes';
import type { MetaData } from 'shared/types/metaDataTypes';

import {
  walletBalanceSchema,
  walletSchema,
} from 'modules/account/store/server/wallet/walletSchemas';

import { getBaseUrl } from 'shared/utils/fetchUtils';

export const fetchWalletApi = async (
  token: string | undefined,
  page: number
): Promise<{ data: Transactions; meta: MetaData } | null> => {
  if (!token) return null;
  try {
    const baseUrl = getBaseUrl();
    const response = await fetch(`${baseUrl}/wallet?size=10&page=${page}`, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const responseData = await response.json();

    const zodData = walletSchema.safeParse(responseData);

    if (zodData.success) {
      const data = zodData.data.data.map((transaction, index) => ({ ...transaction, index }));
      return {
        data,
        meta: zodData.data.meta,
      };
    }

    return null;
  } catch (error) {
    return null;
  }
};

export const fetchWalletBalanceApi = async (
  token: string | undefined
): Promise<WalletBalance['balance'] | null> => {
  if (!token) return null;
  try {
    const baseUrl = getBaseUrl();
    const response = await fetch(`${baseUrl}/wallet/balance`, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    const responseData = await response.json();

    const zodData = walletBalanceSchema.safeParse(responseData);

    if (zodData.success) {
      return zodData.data.balance;
    }

    return null;
  } catch (error) {
    return null;
  }
};

export const postWalletTransactionApi = async (
  token: string | undefined,
  transaction: {
    amount: number;
    type: 'credit' | 'debit';
    cause: string;
  }
): Promise<void> => {
  if (!token) return;
  try {
    const baseUrl = getBaseUrl();
    await fetch(`${baseUrl}/wallet`, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(transaction),
    });
  } catch (err) {
    console.error('Error adding ressource to cart', err);
  }
};
