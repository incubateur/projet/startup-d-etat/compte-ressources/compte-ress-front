import { getBaseUrl } from 'shared/utils/fetchUtils';

export const validateOrder = async (
  token: string | undefined,
  ids: Array<string>
): Promise<void> => {
  if (!token) return;
  try {
    const baseUrl = getBaseUrl();
    ids.forEach(async (id) => {
      await fetch(`${baseUrl}/cart/items/${id}`, {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          status: 'validated',
        }),
      });
    });
  } catch (err) {
    console.error('Error validating order', err);
  }
};

export const purchaseOrder = async (
  token: string | undefined,
  ids: Array<string>
): Promise<void> => {
  if (!token) return;
  try {
    const baseUrl = getBaseUrl();
    ids.forEach(async (id) => {
      await fetch(`${baseUrl}/cart/items/${id}/order`, {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
    });
  } catch (err) {
    console.error('Error validating order', err);
  }
};

export const assignOrder = async (token: string | undefined, ids: Array<string>): Promise<void> => {
  if (!token) return;
  try {
    const baseUrl = getBaseUrl();
    ids.forEach(async (id) => {
      await fetch(`${baseUrl}/cart/items/${id}`, {
        method: 'put',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          status: 'assigned',
        }),
      });
    });
  } catch (err) {
    console.error('Error validating order', err);
  }
};
