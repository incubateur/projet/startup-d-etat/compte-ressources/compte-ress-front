import { useQuery } from '@tanstack/react-query';

import { teacherRequestsStatusEnums } from 'modules/account/enums/accountOrderStatusEnums';
import { ASSIGNED } from 'shared/enums/orderEnums';

import { fetchOrderApi } from 'shared/store/server/fetchOrderApi';

export const useRequestsQuery = (token: string | undefined) =>
  useQuery({
    queryKey: ['teacherRequests'],
    queryFn: () => fetchOrderApi(token, [...teacherRequestsStatusEnums], 1, 200),
    enabled: !!token,
  });

export const useGetAssignedOrdersQuery = (token: string | undefined) =>
  useQuery({
    queryKey: ['assignedOrders'],
    queryFn: () => fetchOrderApi(token, [ASSIGNED], 1, 200),
    enabled: !!token,
  });
