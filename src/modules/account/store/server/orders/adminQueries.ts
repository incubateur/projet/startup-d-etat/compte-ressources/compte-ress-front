import { keepPreviousData, type QueryClient, useMutation, useQuery } from '@tanstack/react-query';

import {
  adminOrdersStatusEnums,
  adminRequestsStatusEnums,
} from 'modules/account/enums/accountOrderStatusEnums';
import { ORDERED, SUBMITTED } from 'shared/enums/orderEnums';

import type {
  AdminOrdersStatus,
  AdminRequestsStatus,
} from 'modules/account/types/accountOrderStatusTypes';
import type { CartItem } from 'modules/cart/types/cartTypes';

import {
  assignOrder,
  purchaseOrder,
  validateOrder,
} from 'modules/account/store/server/orders/adminFetchApi';
import { fetchOrderApi } from 'shared/store/server/fetchOrderApi';

export const useAdminRequestsQuery = (token: string | undefined) =>
  useQuery({
    queryKey: ['adminRequests'],
    queryFn: () => fetchOrderApi(token, [...adminRequestsStatusEnums], 1, 200),
    enabled: !!token,
    placeholderData: keepPreviousData,
  });

export const useSubmittedOrdersQuery = (token: string | undefined) =>
  useQuery({
    queryKey: ['submittedOrders'],
    queryFn: () => fetchOrderApi(token, [SUBMITTED], 1, 200),
    enabled: !!token,
    placeholderData: keepPreviousData,
  });

export const useOrderedOrdersQuery = (token: string | undefined) =>
  useQuery({
    queryKey: ['orderedOrders'],
    queryFn: () => fetchOrderApi(token, [ORDERED], 1, 200),
    enabled: !!token,
    placeholderData: keepPreviousData,
  });

export const useAdminOrdersQuery = (token: string | undefined) =>
  useQuery({
    queryKey: ['adminOrders'],
    queryFn: () => fetchOrderApi(token, [...adminOrdersStatusEnums], 1, 200),
    enabled: !!token,
    placeholderData: keepPreviousData,
  });

export const useOrderValidationMutation = (token: string | undefined, queryClient: QueryClient) =>
  useMutation({
    mutationFn: (ids: Array<string>) => validateOrder(token, ids),
    mutationKey: ['validateOrders'],
    onSuccess: async (_, ids: Array<string>) => {
      await queryClient.cancelQueries({
        queryKey: ['adminRequests'],
      });
      queryClient.setQueriesData<{ data: Array<CartItem>; total: number } | undefined>(
        { queryKey: ['adminRequests'] },
        (old) => {
          if (!old) return old;
          const newData = old.data.map((item) => {
            if (ids.includes(item.id)) {
              return { ...item, status: 'validated' as AdminRequestsStatus };
            }
            return item;
          });
          return {
            data: newData,
            total: old?.total,
          };
        }
      );
      await queryClient.cancelQueries({ queryKey: ['submittedOrders'] });
      queryClient.setQueriesData<{ data: Array<CartItem>; total: number }>(
        { queryKey: ['submittedOrders'] },
        (old) => {
          if (!old) return old;
          return {
            data: old.data.filter((item) => !ids.includes(item.id)),
            total: old?.total,
          };
        }
      );
    },
  });

export const useOrderPurchaseMutation = (token: string | undefined, queryClient: QueryClient) =>
  useMutation({
    mutationFn: (ids: Array<string>) => purchaseOrder(token, ids),
    mutationKey: ['purchaseOrders'],
    onSuccess: async (_, ids: Array<string>) => {
      await queryClient.cancelQueries({ queryKey: ['adminRequests'] });
      queryClient.setQueriesData<{ data: Array<CartItem>; total: number }>(
        { queryKey: ['adminRequests'] },
        (old) => {
          if (!old) return old;
          return {
            data: old.data.filter((item) => !ids.includes(item.id)),
            total: old?.total,
          };
        }
      );
    },
  });

export const useOrderAssignMutation = (token: string | undefined, queryClient: QueryClient) =>
  useMutation({
    mutationFn: (ids: Array<string>) => assignOrder(token, ids),
    mutationKey: ['assignOrders'],
    onSuccess: async (_, ids: Array<string>) => {
      await queryClient.cancelQueries({ queryKey: ['adminOrders'] });
      queryClient.setQueriesData<{ data: Array<CartItem>; total: number }>(
        { queryKey: ['adminOrders'] },
        (old) => {
          if (!old) return old;
          const newData = old.data.map((item) => {
            if (ids.includes(item.id)) {
              return { ...item, status: 'assigned' as AdminOrdersStatus };
            }
            return item;
          });
          return {
            data: newData,
            total: old?.total,
          };
        }
      );
      await queryClient.cancelQueries({ queryKey: ['orderedOrders'] });
      queryClient.setQueryData<{ data: Array<CartItem>; total: number }>(
        ['orderedOrders'],
        (old) => {
          if (!old) return old;
          return {
            data: old.data.filter((item) => !ids.includes(item.id)),
            total: old.data.filter((item) => !ids.includes(item.id)).length,
          };
        }
      );
    },
  });
