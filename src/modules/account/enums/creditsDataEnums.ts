// Valeurs de crédits en fonction de chaque statut

import { ASSIGNED, ORDERED, SUBMITTED, VALIDATED } from 'shared/enums/orderEnums';

import type { ChartConfig } from 'shared/components/molecules/Chart';

export const schoolCreditsPerStatusData = [
  {
    status: SUBMITTED,
    credits: 500,
    fill: `var(--color-${SUBMITTED})`,
  },
  {
    status: VALIDATED,
    credits: 700,
    fill: `var(--color-${VALIDATED})`,
  },
  {
    status: ORDERED,
    credits: 2500,
    fill: `var(--color-${ORDERED})`,
  },
  {
    status: ASSIGNED,
    credits: 3000,
    fill: `var(--color-${ASSIGNED})`,
  },
  {
    status: 'disponible',
    credits: 4500,
    fill: `var(--color-disponible)`,
  },
];
export const creditsPerStatusData = [
  {
    status: SUBMITTED,
    credits: 1000,
    fill: `var(--color-${SUBMITTED})`,
  },
  {
    status: VALIDATED,
    credits: 2000,
    fill: `var(--color-${VALIDATED})`,
  },
  {
    status: ORDERED,
    credits: 3000,
    fill: `var(--color-${ORDERED})`,
  },
  {
    status: ASSIGNED,
    credits: 4000,
    fill: `var(--color-${ASSIGNED})`,
  },
  {
    status: 'disponible',
    credits: 7000,
    fill: `var(--color-disponible)`,
  },
];

export const creditsPerStatusConfig = {
  credits: {
    label: 'Crédits',
  },
  disponible: {
    label: 'Disponible',
    color: 'hsl(var(--chart-1))',
  },
  submitted: {
    label: 'Demandes envoyées',
    color: 'hsl(var(--chart-2))',
  },
  validated: {
    label: 'Demandes validées',
    color: 'hsl(var(--chart-3))',
  },
  ordered: {
    label: 'Commandes livrées',
    color: 'hsl(var(--chart-4))',
  },
  assigned: {
    label: 'Ressources affectées',
    color: 'hsl(var(--chart-5))',
  },
} satisfies ChartConfig;
