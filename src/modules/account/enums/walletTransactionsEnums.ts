export const CREDIT = 'credit';
export const DEBIT = 'debit';

export const transactionTypes = [CREDIT, DEBIT] as const;
