export const ALL = 'all';

export const DATE_ASC = 'dateAsc';
export const DATE_DESC = 'dateDesc';
export const LICENCE_ASC = 'licenceAsc';
export const LICENCE_DESC = 'licenceDesc';

export const adminResourcesSorters = [DATE_ASC, DATE_DESC, LICENCE_ASC, LICENCE_DESC] as const;

export const YEAR_2022 = '2022';
export const YEAR_2023 = '2023';
export const YEAR_2024 = '2024';
export const ALL_YEARS = 'all';

export const yearFilters = [YEAR_2024, YEAR_2023, YEAR_2022] as const;

export const ALL_RESSOURCES = 'all';
export const ACQUIRED_RESOURCES = 'acquired';
export const TEST_RESOURCES = 'test';

export const filterUserResources = [ALL_RESSOURCES, ACQUIRED_RESOURCES, TEST_RESOURCES] as const;
