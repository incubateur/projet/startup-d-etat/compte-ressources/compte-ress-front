import { ASSIGNED, ORDERED, SUBMITTED, VALIDATED } from 'shared/enums/orderEnums';

export const teacherRequestsStatusEnums = [ASSIGNED, ORDERED, VALIDATED, SUBMITTED] as const;
export const adminRequestsStatusEnums = [SUBMITTED, VALIDATED] as const;
export const adminOrdersStatusEnums = [ASSIGNED, ORDERED] as const;
