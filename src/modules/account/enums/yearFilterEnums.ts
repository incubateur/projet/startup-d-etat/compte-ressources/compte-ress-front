export const YEAR_2022 = '2022';
export const YEAR_2023 = '2023';
export const YEAR_2024 = '2024';
export const ALL_YEARS = 'all';

export const filterByYear = [YEAR_2024, YEAR_2023, YEAR_2022];
