import { useNavigate, useSearch } from '@tanstack/react-router';
import { useState } from 'react';

import {
  ACQUIRED_RESOURCES,
  ALL_RESSOURCES,
  TEST_RESOURCES,
} from 'modules/account/enums/accountSorterAndFilterEnums';
import { ADMIN_ROLES } from 'shared/enums/userEnums';

import type { UserResourcesFilter } from 'modules/account/types/accountSorterAndFilterTypes';

import useAuth from 'shared/hooks/useAuth';

import { hasRole } from 'shared/services/userServices';

import { useGetAssignedOrdersQuery } from 'modules/account/store/server/orders/teacherQueries';

import { sliceItemsForPagination } from 'shared/utils/fetchUtils';

const useUserResources = () => {
  const { keycloak, userRoles } = useAuth();

  const [filter, setFilter] = useState<UserResourcesFilter>(ALL_RESSOURCES);
  const search = useSearch({ from: '/_userLayout' });
  const navigate = useNavigate();

  const { data: assignedOrdersData, isLoading: isLoadingOrders } = useGetAssignedOrdersQuery(
    keycloak?.token
  );

  const isAdmin = hasRole(userRoles, [...ADMIN_ROLES]);

  const formattedUserResources = assignedOrdersData?.data
    ?.map((resource) => ({
      ...resource,
      licence: Number(resource.price) % 2 === 0 ? ACQUIRED_RESOURCES : TEST_RESOURCES,
    }))
    .filter((resource) => filter === ALL_RESSOURCES || resource.licence === filter);

  const onFilterChange = async (selectedFilter: UserResourcesFilter) => {
    await navigate({ search: { page: 1 } });
    setFilter(selectedFilter);
  };

  const slicedItems = sliceItemsForPagination(formattedUserResources, search.page, 10);

  return {
    userResources: slicedItems,
    isLoadingOrders,
    isAdmin,
    onFilterChange,
    pagination: {
      pageSize: 10,
      currentPage: search.page,
      total: formattedUserResources?.length || 0,
    },
  };
};

export default useUserResources;
