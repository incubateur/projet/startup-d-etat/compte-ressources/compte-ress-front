import { createModal } from '@codegouvfr/react-dsfr/Modal';
import { useQueryClient } from '@tanstack/react-query';
import { useNavigate, useSearch } from '@tanstack/react-router';
import { useState } from 'react';

import { ALL, DATE_DESC } from 'modules/account/enums/accountSorterAndFilterEnums';
import { ORDERED } from 'shared/enums/orderEnums';
import { ASSIGNMENT_MANAGER } from 'shared/enums/userEnums';

import type {
  AdminOrdersFilter,
  SortingOrder,
} from 'modules/account/types/accountSorterAndFilterTypes';

import useAuth from 'shared/hooks/useAuth';

import { filterAndSortAdminRequests } from 'modules/account/services/adminServices';

import {
  useAdminOrdersQuery,
  useOrderAssignMutation,
} from 'modules/account/store/server/orders/adminQueries';

import { sliceItemsForPagination } from 'shared/utils/fetchUtils';

const modal = createModal({
  id: `modal`,
  isOpenedByDefault: false,
});

const useAdminOrders = () => {
  const { keycloak, userRoles } = useAuth();
  const queryClient = useQueryClient();

  const [selectedFilter, setSelectedFilter] = useState<AdminOrdersFilter>(ALL);
  const [selectedSorter, setSelectedSorter] = useState<SortingOrder>(DATE_DESC);
  const [checkedItems, setCheckedItems] = useState<Array<string>>([]);

  const search = useSearch({ from: '/_userLayout' });
  const navigate = useNavigate();

  const isAssignmentManager = userRoles.includes(ASSIGNMENT_MANAGER);

  const { data: adminOrdersData, isPending } = useAdminOrdersQuery(keycloak?.token);

  const { mutate: assignOrder } = useOrderAssignMutation(keycloak?.token, queryClient);

  const adminOrders = filterAndSortAdminRequests(
    adminOrdersData?.data,
    selectedFilter,
    selectedSorter,
    ORDERED
  );

  const areAllCardsSelected =
    checkedItems.length === adminOrders.filter(({ status }) => status === ORDERED).length;

  const onFilteringChange = async (filter: AdminOrdersFilter) => {
    await navigate({ search: { page: 1 } });
    setSelectedFilter(filter);
  };

  const onSortingChange = async (sorter: SortingOrder) => {
    await navigate({ search: { page: 1 } });
    setSelectedSorter(sorter);
  };

  const onSelectCard = (id: string) => {
    if (checkedItems.includes(id)) {
      setCheckedItems((prevState) => prevState.filter((item) => item !== id));
    } else {
      setCheckedItems((prevState) => [...prevState, id]);
    }
  };

  const onSelectAllCard = () => {
    setCheckedItems(() => {
      if (areAllCardsSelected) {
        return [];
      }
      return adminOrders.filter(({ status }) => status === ORDERED).map(({ id }) => id);
    });
  };

  const onSetAssignedStatusOrder = () => {
    document.querySelector('html')?.removeAttribute('data-fr-scrolling');
    modal.open();
  };

  const onModalConfirmClick = () => {
    assignOrder(checkedItems);
    setCheckedItems([]);
  };

  return {
    selectedFilter,
    selectedSorter,
    adminOrders: sliceItemsForPagination(adminOrders, search.page, 10),
    isPending,
    checkedItems,
    modal,
    areAllCardsSelected,
    isAssignmentManager,
    onFilteringChange,
    onSortingChange,
    onSelectAllCard,
    onSelectCard,
    onSetAssignedStatusOrder,
    onModalConfirmClick,
    pagination: {
      currentPage: search.page,
      pageSize: 10,
      total: adminOrders.length || 0,
    },
  };
};

export default useAdminOrders;
