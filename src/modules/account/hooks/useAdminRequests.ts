import { createModal } from '@codegouvfr/react-dsfr/Modal';
import { useQueryClient } from '@tanstack/react-query';
import { useNavigate, useSearch } from '@tanstack/react-router';
import { useState } from 'react';

import { ALL } from 'modules/account/enums/accountSorterAndFilterEnums';
import { DATE_DESC } from 'modules/catalog/enums/sortingEnums';
import { SUBMITTED } from 'shared/enums/orderEnums';
import { HEAD_OF_SCHOOL } from 'shared/enums/userEnums';

import type {
  AdminRequestsFilter,
  SortingOrder,
} from 'modules/account/types/accountSorterAndFilterTypes';
import type { OrderStatus } from 'shared/types/orderTypes';

import useAuth from 'shared/hooks/useAuth';

import {
  filterAdminRequestsByStatus,
  filterAndSortAdminRequests,
  findAllSelectedOrders,
  findSelectedStatus,
  groupAdminRequests,
} from 'modules/account/services/adminServices';
import { hasRole } from 'shared/services/userServices';

import {
  useAdminRequestsQuery,
  useOrderPurchaseMutation,
  useOrderValidationMutation,
} from 'modules/account/store/server/orders/adminQueries';

import { sliceItemsForPagination } from 'shared/utils/fetchUtils';

const modal = createModal({
  id: `modal`,
  isOpenedByDefault: false,
});

type ModalType = 'validate' | 'order';

const useAdminRequests = () => {
  const { keycloak, userRoles } = useAuth();
  const queryClient = useQueryClient();

  const [selectedFilter, setSelectedFilter] = useState<AdminRequestsFilter>(ALL);
  const [selectedSorter, setSelectedSorter] = useState<SortingOrder>(DATE_DESC);
  const [checkedItems, setCheckedItems] = useState<{ [id: string]: boolean }>({});
  const [modalType, setModalType] = useState<ModalType>('validate');

  const search = useSearch({ from: '/_userLayout' });
  const navigate = useNavigate();

  const { data: adminRequestsData, isLoading: isLoadingRequests } = useAdminRequestsQuery(
    keycloak?.token
  );

  const isDirector = hasRole(userRoles, HEAD_OF_SCHOOL);

  const { mutate: validateOrder } = useOrderValidationMutation(keycloak?.token, queryClient);
  const { mutate: purchaseOrder } = useOrderPurchaseMutation(keycloak?.token, queryClient);

  const adminRequests = filterAndSortAdminRequests(
    adminRequestsData?.data,
    selectedFilter,
    selectedSorter,
    SUBMITTED
  );

  const groupedAdminRequests = groupAdminRequests(adminRequests);

  const onFilteringChange = async (filter: AdminRequestsFilter) => {
    navigate({ search: { page: 1 } });
    setSelectedFilter(filter);
  };

  const onSortingChange = async (sorter: SortingOrder) => {
    navigate({ search: { page: 1 } });
    setSelectedSorter(sorter);
  };

  const onSelectCard = (ark: string, status?: OrderStatus) => {
    const currentRequest = groupedAdminRequests.find(
      (request) => request.ark === ark && request.status === status
    );
    const allOrderIds = currentRequest?.orders.map(({ id }) => id);
    allOrderIds?.forEach((id) => {
      setCheckedItems((prevState) => ({
        ...prevState,
        [id]: !prevState[id],
      }));
    });
  };

  const areAllCardsSelected =
    Object.values(checkedItems).length && Object.values(checkedItems).some((i) => i);

  const onSelectAllCard = () =>
    setCheckedItems((prevState) => findAllSelectedOrders(prevState, adminRequests));

  const onModalOpen = (type: ModalType) => {
    document.querySelector('html')?.removeAttribute('data-fr-scrolling');
    modal.open();
    setModalType(type);
  };

  const onModalConfirmClick = () => {
    const confirmedCheckedItems = filterAdminRequestsByStatus(
      modalType === 'validate' ? 'submitted' : 'validated',
      adminRequests,
      checkedItems
    );
    if (modalType === 'validate') {
      setCheckedItems({});
      return validateOrder(confirmedCheckedItems);
    }
    if (modalType === 'order') {
      setCheckedItems({});
      return purchaseOrder(confirmedCheckedItems);
    }
    return setCheckedItems({});
  };

  const checkedStatus = findSelectedStatus(checkedItems, groupedAdminRequests);

  return {
    adminRequests: sliceItemsForPagination(groupedAdminRequests, search.page, 10),
    checkedItems,
    areAllCardsSelected,
    modal,
    modalType,
    isLoading: isLoadingRequests,
    checkedStatus,
    isDirector,
    onFilteringChange,
    onSortingChange,
    onSelectCard,
    onSelectAllCard,
    onModalOpen,
    onModalConfirmClick,
    pagination: {
      currentPage: search.page,
      pageSize: 10,
      total: groupedAdminRequests?.length || 0,
    },
  };
};

export default useAdminRequests;
