import { useSearch } from '@tanstack/react-router';

import { ADMIN_ROLES } from 'shared/enums/userEnums';

import useAuth from 'shared/hooks/useAuth';
import usePagination from 'shared/hooks/usePagination';

import { hasRole } from 'shared/services/userServices';

import { postWalletTransactionApi } from 'modules/account/store/server/wallet/fetchWalletApi';
import {
  useWalletBalanceQuery,
  useWalletQuery,
} from 'modules/account/store/server/wallet/walletQueries';

const useUserCredits = () => {
  const { keycloak, userRoles } = useAuth();

  const page = useSearch({
    from: '/_userLayout',
    select: (search) => search.page,
  });

  const { data: walletQueryData, refetch } = useWalletQuery(keycloak?.token, page);
  const { onChange, pageSize } = usePagination();

  const {
    data: balanceQueryData,
    refetch: refetchBalance,
    isSuccess,
  } = useWalletBalanceQuery(keycloak?.token);

  const isAdmin = hasRole(userRoles, [...ADMIN_ROLES]);

  const addCredit = async () => {
    await postWalletTransactionApi(keycloak?.token, {
      amount: 100,
      type: 'credit',
      cause: 'test',
    });
    refetch();
    refetchBalance();
  };

  return {
    transactions: walletQueryData?.data,
    balance: balanceQueryData,
    balanceFetched: isSuccess,
    total: walletQueryData?.meta.total_count,
    addCredit,
    page,
    isAdmin,
    pagination: {
      onChange,
      pageSize,
    },
  };
};

export default useUserCredits;
