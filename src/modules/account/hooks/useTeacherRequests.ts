import { useSearch } from '@tanstack/react-router';
import dayjs from 'dayjs';
import { useState } from 'react';

import { ALL_YEARS } from 'modules/account/enums/accountSorterAndFilterEnums';

import useAuth from 'shared/hooks/useAuth';

import { useRequestsQuery } from 'modules/account/store/server/orders/teacherQueries';

import { sliceItemsForPagination } from 'shared/utils/fetchUtils';

const useTeacherRequests = () => {
  const { keycloak } = useAuth();
  const { data: requestsData, isPending } = useRequestsQuery(keycloak?.token);

  const search = useSearch({ from: '/_userLayout' });

  const [filterBy, setFilterByYear] = useState(ALL_YEARS);

  const teacherRequests = requestsData?.data?.filter((item) => {
    if (filterBy === ALL_YEARS) return item;
    const date = dayjs(item.modifiedAt);
    return date.year() === Number(filterBy);
  });

  return {
    teacherRequests: sliceItemsForPagination(teacherRequests, search.page, 10),
    isPending,
    setFilterByYear,
    pagination: {
      pageSize: 10,
      currentPage: search.page,
      total: requestsData?.total || 0,
    },
  };
};

export default useTeacherRequests;
