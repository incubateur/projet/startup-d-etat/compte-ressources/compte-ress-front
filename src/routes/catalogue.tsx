import { createFileRoute } from '@tanstack/react-router';

import type { CatalogSearch } from 'modules/catalog/types/filterTypes';

import CatalogPage from 'modules/catalog/CatalogPage';

type CatalogSearchParams = {
  page?: number;
  gte?: number;
  lt?: number;
};

type CatalogPageParams = CatalogSearch & CatalogSearchParams;

export const Route = createFileRoute('/catalogue')({
  component: CatalogPage,
  validateSearch: (search: CatalogPageParams): CatalogPageParams => ({
    ...search,
    page: Number(search?.page ?? 1),
    gte: Number(search?.gte ?? 0),
    lt: Number(search?.lt ?? 100),
  }),
});
