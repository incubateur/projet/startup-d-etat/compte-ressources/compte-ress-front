import { createFileRoute } from '@tanstack/react-router';

import { TEACHER } from 'shared/enums/userEnums';

import useAuth from 'shared/hooks/useAuth';

import { hasRole } from 'shared/services/userServices';

const Labeling = () => {
  const auth = useAuth();

  if (hasRole(auth.userRoles, TEACHER)) {
    return (
      <div className="0 flex h-[300px] w-full items-center justify-center">
        <p> Page en construction</p>
      </div>
    );
  }

  return (
    <div className="0 flex h-[300px] w-full items-center justify-center">
      Vous n avez pas les droits pour accéder à cette page
    </div>
  );
};

export const Route = createFileRoute('/labellisation')({
  component: Labeling,
});

export default Route;
