import { createFileRoute } from '@tanstack/react-router';

import RessourcePage from 'modules/catalog/components/RessourcePage';

export const Route = createFileRoute('/catalogue/$ressourceId')({
  component: RessourcePage,
});
