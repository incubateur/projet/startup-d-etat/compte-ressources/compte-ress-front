import { createFileRoute } from '@tanstack/react-router';

import CartPage from 'modules/cart/CartPage';

type CartSearch = {
  page: number;
};
export const Route = createFileRoute('/panier')({
  component: CartPage,
  validateSearch: (search: Record<string, unknown>): CartSearch => ({
    page: Number(search?.page ?? 1),
  }),
});
