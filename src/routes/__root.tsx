import { Outlet, createRootRouteWithContext } from '@tanstack/react-router';
import React, { Suspense } from 'react';

import type { UserRole } from 'shared/types/userTypes';

import Footer from 'shared/components/layout/Footer';
import Header from 'shared/components/layout/header/Header';

const LazyTanStackRouterDevtools =
  import.meta.env.MODE === 'production'
    ? () => null
    : React.lazy(() =>
        // Lazy load in development
        import('@tanstack/router-devtools').then((res) => ({
          default: res.TanStackRouterDevtools,
        }))
      );

type MyRouterContext = {
  userRoles: UserRole[];
};

export const Route = createRootRouteWithContext<MyRouterContext>()({
  component: () => (
    <div className="flex min-h-screen w-full flex-col justify-between overflow-x-hidden bg-white">
      <Header />
      <div className="fr-container relative flex grow flex-col">
        <Outlet />
      </div>
      <Suspense>
        <LazyTanStackRouterDevtools />
      </Suspense>
      <Footer />
    </div>
  ),
});

export default Route;
