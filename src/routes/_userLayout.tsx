import { createFileRoute } from '@tanstack/react-router';

import UserLayout from 'shared/components/layout/UserLayout';

type UserCreditsSearch = {
  page: number;
};
export const Route = createFileRoute('/_userLayout')({
  component: UserLayout,
  validateSearch: (search: Record<string, unknown>): UserCreditsSearch => ({
    page: Number(search?.page ?? 1),
  }),
});
