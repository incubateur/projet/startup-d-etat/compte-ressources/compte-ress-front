import { createFileRoute } from '@tanstack/react-router';

import UserResourcesPage from 'modules/account/components/UserResources';

export const Route = createFileRoute('/_userLayout/compte/ressources')({
  component: UserResourcesPage,
});
