import { createFileRoute } from '@tanstack/react-router';

import {
  ASSIGNMENT_MANAGER,
  DIGITAL_REFERENT,
  HEAD_OF_SCHOOL,
  TEACHER,
} from 'shared/enums/userEnums';

import useAuth from 'shared/hooks/useAuth';

import { hasRole } from 'shared/services/userServices';

import AdminRequests from 'modules/account/components/AdminRequests';
import TeacherRequests from 'modules/account/components/TeacherRequests';

const DemandePage = () => {
  const { userRoles } = useAuth();

  if (hasRole(userRoles, [HEAD_OF_SCHOOL, ASSIGNMENT_MANAGER])) {
    return <AdminRequests />;
  }

  if (hasRole(userRoles, [TEACHER, DIGITAL_REFERENT])) {
    return <TeacherRequests />;
  }

  return null;
};

export const Route = createFileRoute('/_userLayout/compte/demandes')({
  component: DemandePage,
});
