import { createFileRoute } from '@tanstack/react-router';

import AccountPage from 'modules/account/AccountPage';

export const Route = createFileRoute('/_userLayout/compte')({
  component: AccountPage,
});
