import { createFileRoute } from '@tanstack/react-router';

import UserCredits from 'modules/account/components/UserCredits';

export const Route = createFileRoute('/_userLayout/compte/credits')({
  component: UserCredits,
});
