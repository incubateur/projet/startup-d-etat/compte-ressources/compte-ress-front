import { createFileRoute } from '@tanstack/react-router';

import AdminOrders from 'modules/account/components/AdminOrders';

export const Route = createFileRoute('/_userLayout/compte/commandes')({
  component: AdminOrders,
});
