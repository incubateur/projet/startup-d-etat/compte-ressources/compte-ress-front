import { createFileRoute } from '@tanstack/react-router';

import UserProfile from 'modules/account/components/UserProfile';

export const Route = createFileRoute('/_userLayout/compte/profil')({
  component: UserProfile,
});
