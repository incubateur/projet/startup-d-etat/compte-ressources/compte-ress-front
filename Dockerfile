FROM node:lts-slim as builder

RUN mkdir -p /app
WORKDIR /app
COPY . /app

ARG TARGET_DEPLOYMENT=$TARGET_DEPLOYMENT

COPY package*.json ./
# RUN yarn && yarn build:$TARGET_DEPLOYMENT
RUN yarn && yarn build

# Destination image
FROM nginx:latest

# Create envConfig.json
RUN echo '{"keycloakUrl": "https://keycloak.tools.lajavaness.com/", "baseUrl": "https://compte-ressource-dev-api.it.lajavaness.com/"}' > /envConfig.json

# overrides default vhost config
COPY .deploy/nginx-vhost.conf /etc/nginx/conf.d/default.conf

# copy /app/build to nginx docroot
COPY --from=builder /app/dist /usr/share/nginx/html

    MAINTAINER Martin Picard <martin@lajavaness.com>
